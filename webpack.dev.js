const path = require('path'),
    configUrl = require('./webpack-url.config.js'),
    BrowserSyncPlugin = require('browser-sync-webpack-plugin'),
    webpack = require('webpack'),
    common = require('./webpack.common.js'),
    merge = require('webpack-merge'),
    KssWebpackPlugin = require('kss-webpack-plugin');

var KssConfig = {
    source: 'resources/source/scss/',
    builder: 'node_modules/kss-scheibo/kss_styleguide/scheibo-template/',
    'css': [
        '../../assets/public.bundle.css',
        '../assets/styleguide.css',
        'https://fonts.googleapis.com/css?family=Lato:300,400,700,900,300i,400,400i&amp;subset=latin-ext',

    ],
    'destination':  'styleguide/toolkit/',
    'js': [


        '../../assetss/public.min.js',
    ],

    'custom'       : ['Colors', 'Wrapper', 'RequireJS', 'BodyClass'],
};





module.exports = merge(common, {
    mode: 'development',
    devtool: 'inline-source-map',

    watch: true,
    watchOptions: {
        aggregateTimeout: 300,
        poll: 1000,
        ignored: /node_modules/,
    },

    plugins: [
        new BrowserSyncPlugin({
            files: './*.php',
            proxy: configUrl.path.vhost,

        }),
        new KssWebpackPlugin(KssConfig),
        // new webpack.SourceMapDevToolPlugin({
        //   filename: '[name].js.map',
        //   exclude: ['vendor.js']
        // })

    ],

});
