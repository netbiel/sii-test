const path = require('path'),
    ExtractTextPlugin = require('extract-text-webpack-plugin'),
    CleanWebpackPlugin = require('clean-webpack-plugin'),
    // SpriteLoaderPlugin = require('svg-sprite-loader/plugin'),
    StyleLintPlugin = require('stylelint-webpack-plugin'),
    webpack = require('webpack'),
    autoprefixer = require('autoprefixer');

module.exports = {
    entry: {
        public: './resources/source/js/main.js',
        company: './resources/source/js/company.js',
        whatWeDo: './resources/source/js/whatWeDo.js',
        industries: './resources/source/js/industries.js',
        career: './resources/source/js/career.js',
        trainings: './resources/source/js/trainings.js',
        newsroom: './resources/source/js/newsroom.js',
    },
    devServer: {
        contentBase: './dist',
        hot: true,
    },
    output: {
        filename: '[name].min.js',
        path: path.resolve(__dirname, 'assets'),
        publicPath: '/',
    },
    module: {
        rules: [
            {
                test: /\.svg$/,
                use: [
                    {
                        loader: 'svg-sprite-loader',
                        options: {
                            extract: true,
                            //spriteFilename: 'sprite-icons.svg'
                        },
                    },
                    'svg-fill-loader',
                    'svgo-loader',
                ],
            },
            //{
            //  enforce: "pre",
            //  test: /\.js$/,
            //  exclude: /node_modules/,
            //  loader: "eslint-loader",
            //  options:{
            //    fix: true
            //  }
            //},
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env'],
                    },
                },
            },

            {
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: ['css-loader',
                        'postcss-loader'],
                }),
            },
            {
                test: /\.scss$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: ['css-loader',
                        'sass-loader',
                        'postcss-loader'],

                }),
            },

            {
                test: /\.(png|svg|jpg|gif)$/,
                use: [{
                    loader: 'file-loader',
                    options: '[name].[ext]',
                },

                ],
            },
            // {
            //              test: /\/.*\.svg$/,
            //              use: [
            //                { loader: 'svg-sprite-loader',
            //                  options: {
            //                    extract: true,
            //                    spriteFilename: './assets/icons.svg',
            //                    runtimeCompat: true} },
            //                  'svg-fill-loader',
            //                  'svgo-loader'
            //                ]
            //
            // }
        ],
        // loaders: [
        //     {
        //         test: /resources\/source\/icons\/.*\.svg$/,
        //         loader: 'svg-sprite-loader',
        //         options: {
        //             extract: true,
        //             spriteFilename: './assets/icons-sprite.svg',
        //             runtimeCompat: true
        //         }
        //     }
        // ]
    },
    plugins: [
        // Adding our UglifyJS plugin
        new CleanWebpackPlugin(['assets/*.*']),
        new ExtractTextPlugin({
            filename: '[name].bundle.css',
        }),
        new StyleLintPlugin({
            files: 'resources/source/scss/screen.scss',
            fix: true,

        }),
        new webpack.LoaderOptionsPlugin({
            options: {
                postcss: [
                    autoprefixer(),
                ],
            },
        }),
        // new SpriteLoaderPlugin({
        //     plainSprite: true
        // }),
    ],
};
