const path = require('path');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const webpack = require('webpack');
// const extractSass = new ExtractTextPlugin({
//     filename: "../css/[name].min.css",
// });

// module.exports = {
//   entry: './resources/source/js/main.js',
//   output: {
//     filename: 'bundle.js',
//     path: path.resolve(__dirname, 'dist')
//   }
// };

module.exports = {
    mode: 'development',
    entry: {
        public: './resources/source/js/main.js',
        company: './resources/source/js/company.js',
        whatWeDo: './resources/source/js/whatWeDo.js',
        industries: './resources/source/js/industries.js',
        career: './resources/source/js/career.js',
        trainings: './resources/source/js/trainings.js',
        newsroom: './resources/source/js/newsroom.js',
    },
    devtool: 'inline-source-map',
    devServer: {
        contentBase: './dist',
        hot: true,
    },
    output: {
        filename: '[name].min.js',
        path: path.resolve(__dirname, 'assets'),
        publicPath: '/',
    },
    watch: true,
    watchOptions: {
        aggregateTimeout: 300,
        poll: 1000,
        ignored: /node_modules/,
    },
    optimization: {
        minimizer: [
            // we specify a custom UglifyJsPlugin here to get source maps in production
            new UglifyJsPlugin({
                cache: true,
                parallel: true,
                sourceMap: true,
            }),
            new OptimizeCSSAssetsPlugin({}),
        ],
    },
    // externals: {
    //   jquery: 'jQuery'
    // },
    plugins: [
    // Adding our UglifyJS plugin
        new CleanWebpackPlugin(['assets/*.*']),
        new ExtractTextPlugin({
            filename: '[name].bundle.css',
        }),
        new BrowserSyncPlugin({
            // browse to http://localhost:3000/ during development,
            // ./public directory is being served
            files: './*.php',
            // host: 'localhost',
            // port: 3000,
            proxy: 'http://localhost/',
            //

        }),
        //  new webpack.optimize.CommonsChunkPlugin({
        //    name: 'public',
        //   filename: 'public.bundle.css'
        // }),

        new webpack.HotModuleReplacementPlugin(),
    //new UglifyJSPlugin(),
    //extractSass
    ],
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env'],
                    },
                },
            },

            {
                test: /\.css$/,
                //   use: [{
                //        loader: "style-loader" // creates style nodes from JS strings
                //    },{
                //        loader: "css-loader" // translates CSS into CommonJS
                //    }]

                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: 'css-loader',
                }),
            },
            {
                test: /\.scss$/,
                use: [{
                    loader: 'style-loader', // creates style nodes from JS strings
                },  {
                    loader: 'sass-loader', // compiles Sass to CSS
                },{
                    loader: 'css-loader ', // translates CSS into CommonJS
                }],
            },
            {
                test: /\.(png|svg|jpg|gif)$/,
                use: [{
                    loader: 'file-loader',
                },

                ],
            },


        ],
    },
};
