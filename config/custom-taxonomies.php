<?php


return [
// Now register the non-hierarchical taxonomy like tag

    'blog_category' => ['blog' =>
        [
        'hierarchical' => true,
        'labels' =>  ['name' => _x( 'Category', 'taxonomy general name' ),
        'singular_name' => _x( 'Category', 'taxonomy singular name' ),
        'search_items' =>  __( 'Search Category' )],
        'show_ui' => true,
        'show_in_rest' => true,
        'show_admin_column' => true,
        'query_var' => true,
        ]
    ],

    'event_category' => ['event' => [
        'hierarchical' => true,
        'labels' => ['name' => _x( 'Category', 'taxonomy general name' ),
        'singular_name' => _x( 'Category', 'taxonomy singular name' ),
        'search_items' =>  __( 'Search Category' )],
        'show_ui' => true,
        'show_in_rest' => true,
        'show_admin_column' => true,
        'query_var' => true
        ]
    ]



];

