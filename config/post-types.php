<?php
return [

    /**
     * Events
     */

    'event' => [
        'labels' => ['name' => _x('Events', 'post type general name', 'sii'),
            'singular_name' => _x('Event', 'post type singular name', 'sii')],
        'description' => __('Description.', 'sii'),
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'event'),
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'supports' => array('title', 'editor', 'author', 'thumbnail', 'excerpt')

    ],

    'case_study' => [

        'labels' => ['name' => _x('Case Studies', 'post type general name', 'sii'),
            'singular_name' => _x('Case study', 'post type singular name', 'sii')],
        'description' => __('Description.', 'sii'),
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'case'),
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'supports' => array('title', 'editor', 'author', 'thumbnail', 'excerpt')
    ],

    'testimonial' => [


    'labels'             =>  ['name'               => _x( 'Testimonials', 'post type general name', 'sii' ),
        'singular_name'      => _x( 'Testimonial', 'post type singular name', 'sii' )],
    'description'        => __( 'Description.', 'sii' ),
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    'rewrite'            => array( 'slug' => 'testimonial' ),
    'capability_type'    => 'post',
    'has_archive'        => true,
    'hierarchical'       => false,
    'menu_position'      => null,
    'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt' )
    ]

];