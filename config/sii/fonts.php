<?php
return [

    'default_font_family' =>
        [
            '"Lato", sans-serif',
            '//fonts.googleapis.com/css?family=Lato:300,400,700,900,300i,400,400i&amp;subset=latin-ext'
        ],
    'secondary_font_family' => ['Montserrat'],
    'third_font_family' => ['Arial'],

];