<?php
return [

    // Section: Logo

    'styles' => [
        'main'        => [
            'path'=>'test.css',
            'deps' => array('default_font_family','slick'), //deps same as option name
            'media' => '',
        ],

        'slick'        => [
            'path'=>'slick.css',
            'deps' => array(), //deps same as option name
            'media' => '',
        ],


    ],

    'scripts' => [
        'main'        => [
            'path'=>'smain-test.js',
            'deps' => ['jquery', 'slick'],
            'in_footer' => true
        ],
        'slick'        => [
            'path'=>'sslick.min.js',
            'deps' => 'jquery',
            'in_footer' => true
        ],
    ]


];