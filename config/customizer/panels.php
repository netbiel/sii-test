<?php
return [

    'pages' => [
        'priority' => 10,
        'title' => esc_html__('Pages', 'kirki'),
        'description' => esc_html__('Pages settings', 'kirki'),
   ],
        'global' => [
    'priority' => 10,
    'title' => esc_html__('Global Settings', 'kirki'),
    'description' => esc_html__('Page global settings', 'kirki'),
]

];