<?php
return [

    'home_page' => [

        'title' => esc_html__('Home Page', 'sii'),
        'description' => esc_html__('My section description.', 'sii'),
        'panel' => 'pages',
        'priority' => 160,

   ],
    'footer' => [

        'title' => esc_html__('Footer', 'sii'),
        'description' => esc_html__('Change footer settings', 'sii'),
        'panel' => 'global',
        'priority' => 160,

    ]

];