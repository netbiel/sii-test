<?php
return [

    'contact_button' => [
        'type' => 'text',
        'settings' => 'contact_button',
        'label' => esc_html__('Text Control', 'sii'),
        'section' => 'home_page',
        'default' => esc_html__('Contact Us', 'sii'),
        'priority' => 10,
        'partial_refresh' => [
            'contact_button' => [
                'selector' => '.sii-o-nav-bar__item__nav-top a',
                'render_callback' => 'wp_get_document_title',
            ],

        ],
    ],

    'home_page_title' => [
        'type' => 'text',
        'settings' => 'home_page_title',
        'label' => esc_html__('Text Control', 'my_textdomain'),
        'section' => 'home_page',
        'default' => esc_html__('Passion for technology', 'my_textdomain'),
        'priority' => 10,
        'partial_refresh' => [
            'home_page_title' => [
                'selector' => '.sii-o-top-lead__header__text h2',
                'render_callback' => 'wp_get_document_title',
            ],

        ],
    ],

    'home_page_describe' => [
        'type' => 'text',
        'settings' => 'home_page_describe',
        'label' => esc_html__('Home Page Description', 'my_textdomain'),
        'section' => 'home_page',
        'default' => esc_html__('With over 4 000 specialists Sii is the top consulting, IT and engineering  services provider in Poland', 'my_textdomain'),
        'priority' => 10,
        'partial_refresh' => [
            'home_page_describe' => [
                'selector' => '.sii-o-top-lead__header__text .sii-a-heading.-t1',

            ],

        ],
    ],

    'footer_logos' => [
        'type'        => 'repeater',
        'label'       => esc_html__( 'Footer Logos', 'kirki' ),
        'section'     => 'footer',
        'priority'    => 10,
        'row_label' => [
            'type'  => 'field',
            'value' => esc_html__('Logo', 'kirki' ),

        ],
        'button_label' => esc_html__('Add logo', 'kirki' ),
        'settings'     => 'footer_logos',

        'fields' => [
            'logo_image' => [
                'type'        => 'image',
                'settings'    => 'logo_footer_url',
                'label'       => esc_html__( 'Image Control (URL)', 'kirki' ),



            ],

        ]
    ]

];