<?php
return [

    'home_page_title' => [
        'selector' => '.sii-o-top-lead__header__text h2',
        'settings' => ['home_page_title'],
        'render_callback' => function () {

            return get_theme_mod('home_page_title', 'display');

        },
    ],
    'contact_button' => [
        'selector' => '.sii-o-top-lead__header__text h2',
        'settings' => ['contact_button'],
        'render_callback' => function () {

            return get_theme_mod('contact_button', 'display');
        },
    ]

];