<?php

    return [
        'menus' =>

            ['main-menu' => __('Main menu', 'sii'),
            'who_we_are' => __('Who we are', 'sii'),
                'industries' => __('Industries', 'sii'),
            'what_we_offer' => __('What we offer', 'sii'),
            'career' => __('Career', 'sii'),
            'trainings' => __('Trainings', 'sii'),
            'newsroom' => __('Newsroom', 'sii'),
            'contact' => __('Contact', 'sii'),
            'social' => __('Social', 'sii')
            ]

    ];