(function($){
    jQuery(function(){

        $('.js-carousel').slick({
            infinite: true,
            slidesToShow: 3,
            variableWidth: true,
            slidesToScroll: 1,
            arrows: false
        });

        $('.js-slide-next').click(function(e){
            e.preventDefault();
            $(".js-carousel").slick('slickNext');
        });

        $('.js-slide-prev').click(function(e){
            e.preventDefault();
            $(".js-carousel").slick('slickPrev');
        });

    }); // end of document ready
})(jQuery); // end of jQuery name space