<!doctype html>
<html {!! get_language_attributes() !!}>
  @include('partials.head')
  <body @php body_class();  sii_body_style() @endphp>
    @php do_action('get_header') @endphp
    @include(App::siteHeaderTemplate());



          @yield('content')




    @php do_action('get_footer') @endphp
    @include('partials.footer')
    @php wp_footer() @endphp
  </body>
</html>
