{{--
  Template Name: Frequently Asked Questions
--}}

@extends('layouts.app')

@section('content')
    @while(have_posts()) @php(the_post())
    @include('partials.page-header')
    @include('partials.content-page')
    @endwhile
    {{ $faq or 'No $faq variable returned' }}<br/>
    {{ $page or 'No $page variable returned' }}



@endsection