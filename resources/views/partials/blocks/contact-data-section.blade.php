<div class="items-icons cnt-section">

    <div class="item">
        <div class="title" style="font-size:25px;">{{$block['items'][0]['header']}}</div>
        <div class="part part1">
            <div class="icon"><span class="dashicons dashicons-location"></span></div>
            <div class="it">
                <p>{{$block['items'][0]['address1']}}</p>
                <p>{{$block['items'][0]['address2']}}</p>
            </div>
        </div>

        <div class="part part2">
            <div class="icon"><span class="dashicons dashicons-phone"></span></div>
            <div class="it">
                <p>{{$block['items'][0]['phone'] }}</p>
                <p>{{$block['items'][0]['email'] }}</p>
            </div>
        </div>


    </div>

</div>