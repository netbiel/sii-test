<div class="sii-o-newsletter">

    <div class="sii-o-newsletter__title">{{$block['blockTitle']}}</div>
    <div class="sii-o-newsletter__subtitle">{{$block['blockSubtitle']}}</div>
    @php echo $partial @endphp

</div>

