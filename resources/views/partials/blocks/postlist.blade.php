
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>


<div class="owl-carousel owl-theme">



   @foreach ($block['images'] as $value)

    <div class="item">
        <div class="title">

            @if ($block['position'])
               {{$value['title']}};
            @endif</div>

        @if ($value['yt'] == false)
        <video width="240" height="240" src="{{$value['url']}}" controls></video>
        @endif

        @if ($value['yt'] == true)
        <iframe width="240" height="240" src="{{$value['url']}}"></iframe>
        @endif

        <div class="title">
            @if ($block['position'] == 1)
                echo $value['title'];
            @endif</div>
        <div class="cover-img">@if ($value['cover'] != '') <img
                    src="{{$value['cover']}}" /> @endif</div>
    </div>

    @endforeach

</div>

<script>
    console.log($('body'));
    $('.owl-carousel').owlCarousel({
        loop: false,
        margin: 10,
        nav: true
    });

    $('.cover-img').click(function () {
        $(this).hide();
        //$(this).parent().trigger('click');
    })

</script>
