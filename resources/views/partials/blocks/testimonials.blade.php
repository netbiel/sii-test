<div class="sii-o-slider -testimonials-large">

    <h3 class="sii-o-slider__title">{{$block['title']}}</h3>

    <div class="sii-o-slider__navigation">
        <ul class="sii-m-nav-arrow -invert sii-o-slider__navigation__arrows">

            <li class="sii-m-nav-arrow__item">
                <a href="#" class="sii-m-nav-arrow__item__link js-slide-prev">
                    <svg class="icon"><use xlink:href="{{App\asset_path('svg/line-awesome/sprite.svg#arrow-left')}}"></use></svg>
                </a>
            </li>
            <li class="sii-m-nav-arrow__item">
                <a href="#" class="sii-m-nav-arrow__item__link js-slide-next">
                    <svg class="icon"><use xlink:href="{{App\asset_path('svg/line-awesome/sprite.svg#arrow-right')}}"></use></svg>
                </a>
            </li>



        </ul>


    </div>

    <ul class="sii-o-slider__content js-carousel">
        <li class="sii-o-slider__content__item -active-slide">
            <div class="sii-o-slider__content__item__testimonial">
                <div class="sii-m-testimonial-item ">
                    <div class="sii-m-testimonial-item__logo">
                        <img src="{{App\asset_path('images/logo-puma.png')}}" width="175" height="94" alt="image"/>
                    </div>
                    <blockquote class="sii-m-testimonial-item__content">
                        <p class="sii-m-testimonial-item__content__quote">Crucial for us is that Sii can provide high-expertise project teams within a relatively short time. Their engineers are experts in their respective fields and contribute to our projects valuable, goal-oriented input and a vast experience,</p>

                        <p class="sii-m-testimonial-item__content__footer">
                            <strong>Kurt Walther,</strong>
                            <span>Senior Head of Global IT, PUMA</span>
                        </p>
                    </blockquote>

                </div>
                <div class="sii-m-info-box ">
                    <div class="sii-m-info-box__content">
                        <h3 class="sii-a-heading -h3 sii-m-info-box__content__title">We are proud to work with</h3>
                        <p class="sii-m-info-box__content__ph2 sii-a-heading -ph2">About:</p>
                        <p>Support in building a product lifecycle management system to organize internal product design and development processes – from concept to final product.</p>
                    </div>
                    <a href="#" class="sii-a-button -secondary -small">
                        Read this story
                    </a>


                </div>
            </div>

        </li>
        <li class="sii-o-slider__content__item -active-slide">
            <div class="sii-o-slider__content__item__testimonial">
                <div class="sii-m-testimonial-item ">
                    <div class="sii-m-testimonial-item__logo">
                        <img src="{{App\asset_path('images/logo-puma.png')}}" width="175" height="94" alt="image"/>
                    </div>
                    <blockquote class="sii-m-testimonial-item__content">
                        <p class="sii-m-testimonial-item__content__quote">Crucial for us is that Sii can provide high-expertise project teams within a relatively short time. Their engineers are experts in their respective</p>

                        <p class="sii-m-testimonial-item__content__footer">
                            <strong>Kurt Walther,</strong>
                            <span>Senior Head of Global IT, PUMA</span>
                        </p>
                    </blockquote>

                </div>
                <div class="sii-m-info-box ">
                    <div class="sii-m-info-box__content">
                        <h3 class="sii-a-heading -h3 sii-m-info-box__content__title">Development of a new product lifecycle management system</h3>
                        <p class="sii-m-info-box__content__ph2 sii-a-heading -ph2">About:</p>
                        <p>Support in building a product lifecycle management system to organize internal product design and development processes – from concept to final product.</p>
                    </div>
                    <a href="#" class="sii-a-button -secondary -small">
                        Read this story
                    </a>


                </div>
            </div>

        </li>
        <li class="sii-o-slider__content__item -active-slide">
            <div class="sii-o-slider__content__item__testimonial">
                <div class="sii-m-testimonial-item ">
                    <div class="sii-m-testimonial-item__logo">
                        <img src="../assets/images/logo-puma.png" width="175" height="94" alt="image"/>
                    </div>
                    <blockquote class="sii-m-testimonial-item__content">
                        <p class="sii-m-testimonial-item__content__quote">Crucial for us is that Sii can provide high-expertise project teams within a relatively short time. Their engineers are experts in their respective</p>

                        <p class="sii-m-testimonial-item__content__footer">
                            <strong>Kurt Walther,</strong>
                            <span>Senior Head of Global IT, PUMA</span>
                        </p>
                    </blockquote>

                </div>
                <div class="sii-m-info-box ">
                    <div class="sii-m-info-box__content">
                        <h3 class="sii-a-heading -h3 sii-m-info-box__content__title">Development of a new product lifecycle management system</h3>
                        <p class="sii-m-info-box__content__ph2 sii-a-heading -ph2">About:</p>
                        <p>Support in building a product lifecycle management system to organize internal product design and development processes – from concept to final product.</p>
                    </div>
                    <a href="#" class="sii-a-button -secondary -small">
                        Read this story
                    </a>


                </div>
            </div>

        </li>


    </ul>

</div>