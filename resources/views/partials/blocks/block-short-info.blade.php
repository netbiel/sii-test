<div class="row">

    @foreach ($block['icon_items'] as $value)


        <div class="col-sm-{{$block['count']}}">
            <div class="sii-m-short-info-box">
                <div class="sii-m-short-info-box__media">
                    <svg>
                        <use xlink:href="{{App\asset_path('svg/line-awesome/sprite.svg#')}}{{$value['icon']['name']}}"/>
                    </svg>
                </div>
                <div class="sii-m-short-info-box__content">
                    <h4 class="sii-m-short-info-box__content__title">{{$value['title']}}</h4>
                    <span class="sii-m-short-info-box__content__subtitle">{{$value['desc']}}</span>
                </div>
            </div>
        </div>
    @endforeach


</div>