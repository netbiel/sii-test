<?php
// don't forget about this for custom templates, or errors will not show for server-side validation
// $zf_error is automatically created by the library and it holds messages about SPAM or CSRF errors
// $error is the name of the variable used with the set_rule method
echo(isset($zf_error) ? $zf_error : (isset($error) ? $error : ''));


echo isset($course_nonce) ? $course_nonce : '';

echo isset($frm_xpnd) ? $frm_xpnd : '';



?>

<div class="sii-o-newsletter__form">
    <div class="sii-m-input-line sii-o-newsletter__form__input">
        <?php echo $email ?>
        <?php echo $label_email; ?>
    </div>



        <button class="sii-a-badge-button sii-o-newsletter__form__button" type="button"><svg class="icon icon-arrow-right2">
                <use
                    xlink:href="<?php echo App\asset_path('svg/font-awesome-sprites/solid.svg#arrow-right') ?>"></use>
            </svg></button>


</div>
<div>

    <div class="sii-m-checkbox-line sii-o-newsletter__checkbox">
        <?php echo $contact_email_yes ?>

        <?php echo $label_contact_email ?>
    </div>
</div>
