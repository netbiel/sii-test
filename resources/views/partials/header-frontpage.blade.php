<section class="sii-t-section u-move-over">
    <div class="container">
        <div class="sii-o-top-lead -home">

            <div class="sii-o-nav-bar">
                {{ sii_brand_logo() }}

                <div class="sii-o-nav-bar__item">



                    <div class="sii-o-nav-bar__item__nav-top">

                        <div class="sii-o-nav-bar__item__nav-top__language-select">{!!do_action('wpml_add_language_selector')!!}</div>

                        <a href="#" class="sii-a-button -small -secondary">
                            {{get_theme_mod( 'contact_button_'.ICL_LANGUAGE_CODE)}}
                        </a>


                    </div>
                    <div class="sii-o-nav-bar__item__nav-main">
                        @if (has_nav_menu('main-menu'))
                            {!! sii_nav_menu(['theme_location' => 'main-menu', 'menu_class' => '-main-menu
                            -invert', 'depth' => 1])
                            !!}
                        @endif
                    </div>
                </div>
            </div>


            <div class="sii-o-top-lead__header">
                <div class="sii-o-top-lead__header__text u-move-over">
                    <h2 class="sii-o-top-lead__header__text__title sii-a-heading -h0">{{ get_theme_mod('home_page_title_'.ICL_LANGUAGE_CODE) }}</h2>
                    <p class="sii-a-heading -t1 sii-o-top-lead__header__text__subtitle">{{ get_theme_mod('home_page_describe_'.ICL_LANGUAGE_CODE) }}</p>
                    <div class="sii-m-btn-group ">
                        <li class="sii-m-btn-group__item">
                            <a href="#" class="sii-a-button -secondary">
                                Send Your Request
                            </a>


                        </li>
                        <li class="sii-m-btn-group__item">
                            <a href="#" class="sii-a-button -primary">
                                Join Sii
                            </a>


                        </li>
                    </div>
                    <a href="#" class="sii-o-top-lead__header__text__go-down sii-a-badge-button -only-icon -invert">
                        <svg class="icon"><use xlink:href="{{App\asset_path('svg/line-awesome/sprite.svg#arrow-circle-o-down')}}"></use></svg>
                    </a>
                </div>
                <div class="sii-o-top-lead__header__utilities">

                    <span class="sii-o-top-lead__header__utilities__caption">
                        Remek, Test & Analysis Engineer
                    </span>



                </div>
            </div>
        </div>

    </div>
</section>