
<div class="sii-t-section -third-gradient-section u-move-under">
    @include('partials.components.offer-menu')
</div>

<div class="sii-t-section -content-section -third-color-section">
    <div class="container">
        <header class="sii-m-section-header -invert" style="margin-bottom: 80px">

            <h2 class="sii-m-section-header__title sii-a-heading -h2">Who we are</h2>
            <p class="sii-m-section-header__subtitle sii-a-heading -t2">The fastest growing Polish consulting, IT and engineering services company</p>

        </header>

        <div class="row">
            <div class="col-sm-4">
                <div class="sii-m-short-info-box ">
                    <div class="sii-m-short-info-box__media">
                        <svg><use xlink:href="{{App\asset_path('svg/line-awesome/sprite.svg#users')}}"></use></svg>
                    </div>
                    <div class="sii-m-short-info-box__content">
                        <h4 class="sii-m-short-info-box__content__title"><span class="js-number-counter">4 000</span> Specialists</h4>
                        <span class="sii-m-short-info-box__content__subtitle">and still growing</span>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="sii-m-short-info-box ">
                    <div class="sii-m-short-info-box__media">
                        <svg><use xlink:href="{{App\asset_path('svg/line-awesome/sprite.svg#money')}}"></use></svg>
                    </div>
                    <div class="sii-m-short-info-box__content">
                        <h4 class="sii-m-short-info-box__content__title">PLN <span class="js-number-counter">675</span>M</h4>
                        <span class="sii-m-short-info-box__content__subtitle">annual revenue</span>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="sii-m-short-info-box ">
                    <div class="sii-m-short-info-box__media">
                        <svg><use xlink:href="{{App\asset_path('svg/line-awesome/sprite.svg#rocket')}}"></use></svg>
                    </div>
                    <div class="sii-m-short-info-box__content">
                        <h4 class="sii-m-short-info-box__content__title">Since 2006</h4>
                        <span class="sii-m-short-info-box__content__subtitle">on the market</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="sii-t-section -content-section -third-color-section -invert-bottom">


    <div class="sii-t-section__asymetric-container">
<div class="sii-o-slider -movies" style="padding-bottom: 20px">

    <h3 class="sii-o-slider__title">Sii Great Place to Work 2018</h3>

    <div class="sii-o-slider__navigation">
        <ul class="sii-m-nav-arrow -secondary sii-o-slider__navigation__arrows">

            <li class="sii-m-nav-arrow__item">
                <a href="#" class="sii-m-nav-arrow__item__link js-slide-prev">
                    <svg class="icon"><use xlink:href="{{App\asset_path('svg/line-awesome/sprite.svg#arrow-left')}}"></use></svg>
                </a>
            </li>
            <li class="sii-m-nav-arrow__item">
                <a href="#" class="sii-m-nav-arrow__item__link js-slide-next">
                    <svg class="icon"><use xlink:href="{{App\asset_path('svg/line-awesome/sprite.svg#arrow-right')}}"></use></svg>
                </a>
            </li>



        </ul>

        <a href="#" class="sii-a-button -small">
            See all videos
        </a>


    </div>

    <ul class="sii-o-slider__content js-carousel">
        <li class="sii-o-slider__content__item">
            <h4>Road to success Sii</h4>
            <div class="sii-m-video-image">
                <a href="#" class="sii-m-video-image__link">
                    <img src="{{App\asset_path('images/image-video.jpg')}}" alt="Image" />
                </a>
                <a href="#" class="sii-a-badge-button sii-m-video-image__play">
                    <svg class="icon icon-arrow-right2"><use xlink:href="{{App\asset_path('svg/font-awesome-sprites/solid.svg#play')}}"></use></svg>
                </a>


            </div>
        </li>
        <li class="sii-o-slider__content__item">
            <h4>Road to success Sii</h4>
            <div class="sii-m-video-image ">
                <a href="#" class="sii-m-video-image__link">
                    <img src="{{App\asset_path('images/image-video.jpg')}}" alt="Image" />
                </a>
                <a href="#" class="sii-a-badge-button sii-m-video-image__play">
                    <svg class="icon icon-arrow-right2"><use xlink:href="{{App\asset_path('svg/font-awesome-sprites/solid.svg#play')}}"></use></svg>
                </a>


            </div>
        </li>
        <li class="sii-o-slider__content__item">
            <h4>Sii Your Future</h4>
            <div class="sii-m-video-image ">
                <a href="#" class="sii-m-video-image__link">
                    <img src="{{App\asset_path('images/image-video.jpg')}}" alt="Image" />
                </a>
                <a href="#" class="sii-a-badge-button sii-m-video-image__play">
                    <svg class="icon icon-arrow-right2"><use xlink:href="{{App\asset_path('svg/font-awesome-sprites/solid.svg#play')}}"></use></svg>
                </a>


            </div>
        </li>
        <li class="sii-o-slider__content__item">
            <h4>An interview with Sabre</h4>
            <div class="sii-m-video-image ">
                <a href="#" class="sii-m-video-image__link">
                    <img src="{{App\asset_path('images/image-video.jpg')}}" alt="Image" />
                </a>
                <a href="#" class="sii-a-badge-button sii-m-video-image__play">
                    <svg class="icon icon-arrow-right2"><use xlink:href="{{App\asset_path('svg/font-awesome-sprites/solid.svg#play')}}"></use></svg>
                </a>


            </div>
        </li>
        <li class="sii-o-slider__content__item">
            <h4>Road to success Sii</h4>
            <div class="sii-m-video-image ">
                <a href="#" class="sii-m-video-image__link">
                    <img src="{{App\asset_path('mages/image-video.jpg')}}" alt="Image" />
                </a>
                <a href="#" class="sii-a-badge-button sii-m-video-image__play">
                    <svg class="icon icon-arrow-right2"><use xlink:href="{{App\asset_path('svg/font-awesome-sprites/solid.svg#play')}}"></use></svg>
                </a>


            </div>
        </li>
        <li class="sii-o-slider__content__item">
            <h4>An interview with Sabre</h4>
            <div class="sii-m-video-image ">
                <a href="#" class="sii-m-video-image__link">
                    <img src="{{App\asset_path('images/image-video.jpg')}}" alt="Image" />
                </a>
                <a href="#" class="sii-a-badge-button sii-m-video-image__play">
                    <svg class="icon icon-arrow-right2"><use xlink:href="{{App\asset_path('svg/font-awesome-sprites/solid.svg#play')}}"></use></svg>
                </a>


            </div>
        </li>

    </ul>

</div>
</div>
    </div>
<div class="sii-t-section -content-section -invert-section">

    <div class="container">

        <header class="sii-m-section-header">
            <p class="sii-m-section-header__subtitle sii-a-heading -t2">Certificates and awards</p>
        </header>
        <ul class="sii-m-column-list ">
            <li class="sii-m-column-list__item " >
                <img src="{{App\asset_path('images/logos/cc-copy.png')}}" alt="image" />
            </li>
            <li class="sii-m-column-list__item ">
                <img src="{{App\asset_path('images/logos/iso2-copy.png')}}" alt="image" />
            </li>
            <li class="sii-m-column-list__item ">
                <img src="{{App\asset_path('images/logos/iso-copy.png')}}" alt="image" />
            </li>
            <li class="sii-m-column-list__item ">
                <img src="{{App\asset_path('images/logo-deal.png')}}" alt="image" />
            </li>


        </ul>
    </div>

</div>

<div class="sii-t-section -content-section -primary-color-section">
    <div class="sii-t-section__asymetric-container">
        <div class="sii-o-slider -testimonials-large" style="padding-bottom:10px">

            <h3 class="sii-o-slider__title">We are proud to work with</h3>

            <div class="sii-o-slider__navigation">
                <ul class="sii-m-nav-arrow -invert sii-o-slider__navigation__arrows">

                    <li class="sii-m-nav-arrow__item">
                        <a href="#" class="sii-m-nav-arrow__item__link js-slide-prev">
                            <svg class="icon"><use xlink:href="{{App\asset_path('svg/line-awesome/sprite.svg#arrow-left')}}"></use></svg>
                        </a>
                    </li>
                    <li class="sii-m-nav-arrow__item">
                        <a href="#" class="sii-m-nav-arrow__item__link js-slide-next">
                            <svg class="icon"><use xlink:href="{{App\asset_path('svg/line-awesome/sprite.svg#arrow-right')}}"></use></svg>
                        </a>
                    </li>



                </ul>


            </div>

            <ul class="sii-o-slider__content js-carousel">
                <li class="sii-o-slider__content__item -active-slide">
                    <div class="sii-o-slider__content__item__testimonial">
                        <div class="sii-m-testimonial-item ">
                            <div class="sii-m-testimonial-item__logo">
                                <img src="{{App\asset_path('images/logo-puma.png')}}" width="175" height="94" alt="image"/>
                            </div>
                            <blockquote class="sii-m-testimonial-item__content">
                                <p class="sii-m-testimonial-item__content__quote">Crucial for us is that Sii can provide high-expertise project teams within a relatively short time. Their engineers are experts in their respective fields and contribute to our projects valuable, goal-oriented input and a vast experience,</p>

                                <p class="sii-m-testimonial-item__content__footer">
                                    <strong>Kurt Walther,</strong>
                                    <span>Senior Head of Global IT, PUMA</span>
                                </p>
                            </blockquote>

                        </div>
                        <div class="sii-m-info-box ">
                            <div class="sii-m-info-box__content">
                                <h3 class="sii-a-heading -h3 sii-m-info-box__content__title">We are proud to work with</h3>
                                <p class="sii-m-info-box__content__ph2 sii-a-heading -ph2">About:</p>
                                <p>Support in building a product lifecycle management system to organize internal product design and development processes – from concept to final product.</p>
                            </div>
                            <a href="#" class="sii-a-button -secondary -small">
                                Read this story
                            </a>


                        </div>
                    </div>

                </li>
                <li class="sii-o-slider__content__item -active-slide">
                    <div class="sii-o-slider__content__item__testimonial">
                        <div class="sii-m-testimonial-item ">
                            <div class="sii-m-testimonial-item__logo">
                                <img src="{{App\asset_path('images/logo-puma.png')}}" width="175" height="94" alt="image"/>
                            </div>
                            <blockquote class="sii-m-testimonial-item__content">
                                <p class="sii-m-testimonial-item__content__quote">Crucial for us is that Sii can provide high-expertise project teams within a relatively short time. Their engineers are experts in their respective</p>

                                <p class="sii-m-testimonial-item__content__footer">
                                    <strong>Kurt Walther,</strong>
                                    <span>Senior Head of Global IT, PUMA</span>
                                </p>
                            </blockquote>

                        </div>
                        <div class="sii-m-info-box ">
                            <div class="sii-m-info-box__content">
                                <h3 class="sii-a-heading -h3 sii-m-info-box__content__title">Development of a new product lifecycle management system</h3>
                                <p class="sii-m-info-box__content__ph2 sii-a-heading -ph2">About:</p>
                                <p>Support in building a product lifecycle management system to organize internal product design and development processes – from concept to final product.</p>
                            </div>
                            <a href="#" class="sii-a-button -secondary -small">
                                Read this story
                            </a>


                        </div>
                    </div>

                </li>
                <li class="sii-o-slider__content__item -active-slide">
                    <div class="sii-o-slider__content__item__testimonial">
                        <div class="sii-m-testimonial-item ">
                            <div class="sii-m-testimonial-item__logo">
                                <img src="../assets/images/logo-puma.png" width="175" height="94" alt="image"/>
                            </div>
                            <blockquote class="sii-m-testimonial-item__content">
                                <p class="sii-m-testimonial-item__content__quote">Crucial for us is that Sii can provide high-expertise project teams within a relatively short time. Their engineers are experts in their respective</p>

                                <p class="sii-m-testimonial-item__content__footer">
                                    <strong>Kurt Walther,</strong>
                                    <span>Senior Head of Global IT, PUMA</span>
                                </p>
                            </blockquote>

                        </div>
                        <div class="sii-m-info-box ">
                            <div class="sii-m-info-box__content">
                                <h3 class="sii-a-heading -h3 sii-m-info-box__content__title">Development of a new product lifecycle management system</h3>
                                <p class="sii-m-info-box__content__ph2 sii-a-heading -ph2">About:</p>
                                <p>Support in building a product lifecycle management system to organize internal product design and development processes – from concept to final product.</p>
                            </div>
                            <a href="#" class="sii-a-button -secondary -small">
                                Read this story
                            </a>


                        </div>
                    </div>

                </li>


            </ul>

        </div>



    </div>
</div>



<div class="sii-t-section -content-section -primary-color-section -invert-bottom">
    <div class="sii-t-section__asymetric-container">
    <div class="sii-o-slider -clients">

        <h3 class="sii-o-slider__title" style="font-weight:300">We are trusted by various industries</h3>

        <div class="sii-o-slider__navigation">
            <ul class="sii-m-nav-arrow -invert sii-o-slider__navigation__arrows">

                <li class="sii-m-nav-arrow__item">
                    <a href="#" class="sii-m-nav-arrow__item__link">
                        <svg class="icon"><use xlink:href="{{App\asset_path('svg/line-awesome/sprite.svg#arrow-left')}}"></use></svg>
                    </a>
                </li>
                <li class="sii-m-nav-arrow__item">
                    <a href="#" class="sii-m-nav-arrow__item__link">
                        <svg class="icon"><use xlink:href="{{App\asset_path('svg/line-awesome/sprite.svg#arrow-right')}}"></use></svg>
                    </a>
                </li>



            </ul>


        </div>
        <div class="sii-o-slider__tabs">
            <ul id="menu-bottom-menu" class="sii-m-nav-tabs -invert sii-o-slider__navigation__arrows">
                <li class="sii-m-nav-tabs__item -selected-item">
                    <a href="#" class="sii-m-nav-tabs__item__link">Selected</a>
                </li>
                <li class="sii-m-nav-tabs__item">
                    <a href="#" class="sii-m-nav-tabs__item__link">Automotive</a>
                </li>
                <li class="sii-m-nav-tabs__item">
                    <a href="#" class="sii-m-nav-tabs__item__link">Banking &amp; insurance</a>
                </li>
                <li class="sii-m-nav-tabs__item">
                    <a href="#" class="sii-m-nav-tabs__item__link">Hi-tech &amp; Engineering</a>
                </li>
                <li class="sii-m-nav-tabs__item">
                    <a href="#" class="sii-m-nav-tabs__item__link">Media &amp; communication</a>
                </li>
                <li class="sii-m-nav-tabs__item">
                    <a href="#" class="sii-m-nav-tabs__item__link">Medical</a>
                </li>
                <li class="sii-m-nav-tabs__item">
                    <a href="#" class="sii-m-nav-tabs__item__link">Oil &amp; gas</a>
                </li>
                <li class="sii-m-nav-tabs__item">
                    <a href="#" class="sii-m-nav-tabs__item__link">Public</a>
                </li>
                <li class="sii-m-nav-tabs__item">
                    <a href="#" class="sii-m-nav-tabs__item__link">Retail &amp; transport</a>
                </li>


            </ul>

        </div>

        <ul class="sii-m-column-list sii-o-slider__content">
            <li class="sii-m-column-list__item" >
                <div class="sii-m-card-logo -small">
                    <div class="sii-m-card-logo__image">
                        <img src="{{App\asset_path('images/logo-puma.png')}}" alt="image" />
                    </div>

                </div>
            </li>
            <li class="sii-m-column-list__item">
                <div class="sii-m-card-logo -small">
                    <div class="sii-m-card-logo__image">
                        <img src="{{App\asset_path('images/logo-aig.png')}}" alt="image" />
                    </div>
                </div>
            </li>
            <li class="sii-m-column-list__item">
                <div class="sii-m-card-logo -small">
                    <div class="sii-m-card-logo__image">
                        <img src="{{App\asset_path('images/logo-bombardier.png')}}" alt="image" />
                    </div>
                </div>
            </li>
            <li class="sii-m-column-list__item">
                <div class="sii-m-card-logo -small">
                    <div class="sii-m-card-logo__image">
                        <img src="{{App\asset_path('images/logo-deal.png')}}" alt="image" />
                    </div>
                </div>
            </li>
            <li class="sii-m-column-list__item">
                <div class="sii-m-card-logo -small">
                    <div class="sii-m-card-logo__image">
                        <img src="{{App\asset_path('images/logo-bombardier.png')}}" alt="image" />
                    </div>
                </div>
            </li>
            <li class="sii-m-column-list__item">
                <div class="sii-m-card-logo -small">
                    <div class="sii-m-card-logo__image">
                        <img src="{{App\asset_path('images/logo-deal.png')}}" alt="image" />
                    </div>
                </div>
            </li>

        </ul>

        <a href="#" class="sii-a-icon-button sii-o-slider__more -small -invert">
        <span>
        All case studies
    </span><svg class="icon icon-arrow-right2"><use xlink:href="{{App\asset_path('svg/line-awesome/sprite.svg#arrow-right')}}"></use></svg>
        </a>



    </div>
</div>

    <div class="container">
        <h3 style="color: #fff; margin-bottom: 50px;font-size:40px">Learn more about us</h3>
        <div class="sii-o-news-box">

            <h3 class="sii-o-news-box__box-title">See what's new in Sii</h3>
            <div class="row">

                <div class="col-sm-4">
                    <h4 class="sii-o-news-box__column-title">Upcoming events</h4>
                    <div class="sii-m-card-item ">
                        <div class="sii-m-card-item__image">
                            <img src="{{App\asset_path('images/image-1.jpg')}}" width="465" height="365" alt="image" />
                        </div><div class="sii-m-card-item__content">
                            <span class="sii-a-chips-label -invert">Blog</span>




                            <h3 class="sii-m-card-item__content__title">Factory of the Future</h3>
                            <a href="#" class="sii-a-button -secondary -small">
                                Read More
                            </a>



                        </div>
                    </div>

                </div>
                <div class="col-sm-4">
                    <h4 class="sii-o-news-box__column-title">Most recent blog posts</h4>
                    <div class="sii-m-card-item ">
                        <div class="sii-m-card-item__image">
                            <img src="{{App\asset_path('images/image-1.jpg')}}" width="465" height="365" alt="image" />
                        </div><div class="sii-m-card-item__content">
                            <span class="sii-a-chips-label -invert">Blog</span>




                            <h3 class="sii-m-card-item__content__title">Factory of the Future</h3>
                            <a href="#" class="sii-a-button -secondary -small">
                                Read More
                            </a>



                        </div>
                    </div>

                </div>
                <div class="col-sm-4">
                    <h4 class="sii-o-news-box__column-title">&nbsp;</h4>
                    <div class="sii-m-badge-item sii-o-news-box__item">
                        <div class="sii-m-badge-item__icon">
                            <a href="#" class="sii-a-badge-button -invert">
                                <svg class="icon icon-arrow-right2"><use xlink:href="{{App\asset_path('svg/line-awesome/sprite.svg#arrow-right')}}"></use></svg>
                            </a>


                        </div>
                        <div class="sii-m-badge-item__content">
                            <span class="sii-a-chips-label sii-o-news-box__item">Blog</span>




                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        </div>
                    </div>
                    <div class="sii-m-badge-item sii-o-news-box__item">
                        <div class="sii-m-badge-item__icon">
                            <a href="#" class="sii-a-badge-button -invert">
                                <svg class="icon icon-arrow-right2"><use xlink:href="{{App\asset_path('svg/line-awesome/sprite.svg#arrow-right')}}"></use></svg>
                            </a>


                        </div>
                        <div class="sii-m-badge-item__content">
                            <span class="sii-a-chips-label sii-o-news-box__item">Blog</span>




                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        </div>
                    </div>
                    <div class="sii-m-badge-item sii-o-news-box__item">
                        <div class="sii-m-badge-item__icon">
                            <a href="#" class="sii-a-badge-button -invert">
                                <svg class="icon icon-arrow-right2"><use xlink:href="{{App\asset_path('svg/line-awesome/sprite.svg#arrow-right')}}"></use></svg>
                            </a>


                        </div>
                        <div class="sii-m-badge-item__content">
                            <span class="sii-a-chips-label sii-o-news-box__item">Blog</span>




                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        </div>
                    </div>

                </div>
            </div>
            <a href="#" class="sii-a-icon-button -small sii-o-news-box__more">
        <span>
        More news
    </span><svg class="icon icon-arrow-right2"><use xlink:href="{{App\asset_path('svg/line-awesome/sprite.svg#arrow-right')}}"></use></svg>
            </a>


        </div>


    </div>
</div>
<div class="sii-t-section -content-section -primary-color-section -invert-color-section" style="padding-bottom: 150px">
    <div class="container">
<div class="sii-o-newsletter">

    <div class="sii-o-newsletter__title">Don't miss out</div>
    <div class="sii-o-newsletter__subtitle">Subscribe to our newsletter and keep up to date with the latest news from Sii.</div>
    <div class="sii-o-newsletter__form">
        <div class="sii-m-input-line sii-o-newsletter__form__input">
            <span class="sii-m-input-line__error">This field must contain only letters and special characters</span>
            <input type="text" id="text" name="scales" required="">
            <label for="text">Text Label</label>
        </div>
        <a href="#" class="sii-a-badge-button sii-o-newsletter__form__button">
            <svg class="icon icon-arrow-right2"><use xlink:href="{{App\asset_path('svg/line-awesome/sprite.svg#arrow-right')}}"></use></svg>
        </a>


    </div>
    <div class="sii-m-checkbox-line sii-o-newsletter__checkbox">
        <input type="checkbox" id="scales" name="scales" checked="">
        <label for="scales">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</label>
    </div>
</div>
    </div>
</div>
@php

//echo sii_do_blocks ($post->post_content);

 @endphp
{!! wp_link_pages(['echo' => 0, 'before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']) !!}
