<footer class="content-info">


    <section class="sii-t-section -third-color-section">
        <div class="container">
            <div class="sii-o-footer-bar ">


                <div class="sii-o-footer-bar__column">

                    @if (has_nav_menu('who_we_are'))
                        <h4 class="sii-o-footer-bar__column__title">{{_e('Who we are', 'sii')}}</h4>
                        {!! sii_nav_menu(['theme_location' => 'who_we_are', 'menu_class' => '-vertical-menu -invert -small', 'depth' => 1])
                        !!}
                    @endif


                </div>
                <div class="sii-o-footer-bar__column">



                    @if (has_nav_menu('what_we_offer'))
                        <h4 class="sii-o-footer-bar__column__title">{{_e('What we offer', 'sii')}}</h4>
                        {!! sii_nav_menu(['theme_location' => 'what_we_offer', 'menu_class' => '-vertical-menu -invert -small', 'depth' => 1])
                        !!}
                    @endif


                </div>
                <div class="sii-o-footer-bar__column">



                    @if (has_nav_menu('industries'))
                        <h4 class="sii-o-footer-bar__column__title">{{_e('Industries', 'sii')}}</h4>
                        {!! sii_nav_menu(['theme_location' => 'industries', 'menu_class' => '-vertical-menu -invert -small', 'depth' => 1])
                        !!}
                    @endif


                </div>
                <div class="sii-o-footer-bar__column">


                    @if (has_nav_menu('career'))
                        <h4 class="sii-o-footer-bar__column__title">{{_e('Career', 'sii')}}</h4>

                        {!! sii_nav_menu(['theme_location' => 'career', 'menu_class' => '-vertical-menu -invert -small', 'depth' => 1])
                        !!}
                    @endif


                </div>
                <div class="sii-o-footer-bar__column">


                    @if (has_nav_menu('trainings'))
                        <h4 class="sii-o-footer-bar__column__title">{{_e('Trainings', 'sii')}}</h4>
                        {!! sii_nav_menu(['theme_location' => 'trainings', 'menu_class' => '-vertical-menu -invert -small', 'depth' => 1])
                        !!}
                    @endif


                </div>
                <div class="sii-o-footer-bar__column">


                    @if (has_nav_menu('newsroom'))
                        <h4 class="sii-o-footer-bar__column__title">{{_e('News', 'sii')}}</h4>
                        {!! sii_nav_menu(['theme_location' => 'newsroom', 'menu_class' => '-vertical-menu -invert -small', 'depth' => 1])
                        !!}
                    @endif


                </div>

                <div class="sii-o-footer-bar__column">
                    @if (has_nav_menu('contact'))
                    <h4 class="sii-o-footer-bar__column__title">{{_e('Contact', 'sii')}}</h4>

                        {!! sii_nav_menu(['theme_location' => 'contact', 'menu_class' => '-vertical-menu -invert -small', 'depth' => 1])
                        !!}
                    @endif
                    @if (has_nav_menu('social'))
                    <h4 class="sii-o-footer-bar__column__title">{{_e('Social', 'sii')}}</h4>

                        {!! sii_nav_menu(['theme_location' => 'social', 'menu_class' => '-vertical-menu -invert -small', 'depth' => 1])
                        !!}
                    @endif


                </div>

            </div>

        </div>
    </section>
    <section class="sii-t-section -invert-section">

        <div class="container" style="padding:30px 0">
            <ul class="sii-m-column-list">
                <?php

                $logos = get_theme_mod('footer_logos_' . ICL_LANGUAGE_CODE);



                if(is_array($logos)) {


                foreach($logos as $logo ){

                    $src = wp_get_attachment_image_src($logo['logo_image'], 'full')

                ?>
                <li class="sii-m-column-list__item"><img src="<?php echo $src[0] ?>" alt="logo" style="max-height: 50px" /></li>

                <?php
                }

                }

                    ?>
            </ul>
        </div>

    </section>
</footer>
