<div class="sii-o-menu-rounded-large">
    <div class="sii-o-menu-rounded-large__container">
        <div class="sii-o-menu-rounded-large__content">
            <div class="sii-o-menu-rounded-large__content__title">
                <h2>{{_e('Our expertise', 'sii')}}</h2>
            </div>
        </div>
        <div class="sii-o-menu-rounded-large__list">
            <div class="sii-o-menu-rounded-large__list__item">
                <div class="sii-m-badge-menu-item ">
                    <a href="#" class="sii-m-badge-menu-item__link">
                        <span class="sii-a-badge-button -icon-default -invert sii-m-badge-menu-item__link__icon"><svg>
                                <use xlink:href="{{sii_icon_path('book')}}"></use>
                            </svg></span>
                        <span class="sii-a-badge-button sii-m-badge-menu-item__link__icon-hover"><svg>
                                <use xlink:href="{{sii_icon_path('arrow-right')}}"></use>
                            </svg></span>
                        <span class="sii-m-badge-menu-item__link__title">Digital</span>
                    </a>
                </div>


            </div>
            <div class="sii-o-menu-rounded-large__list__item">
                <div class="sii-m-badge-menu-item ">
                    <a href="#" class="sii-m-badge-menu-item__link">
                        <span class="sii-a-badge-button -icon-default -invert sii-m-badge-menu-item__link__icon"><svg>
                                <use xlink:href="{{App\asset_path('svg/font-awesome-sprites/solid.svg#book')}}"></use>
                            </svg></span>
                        <span class="sii-a-badge-button sii-m-badge-menu-item__link__icon-hover"><svg>
                                <use xlink:href="{{App\asset_path('svg/font-awesome-sprites/solid.svg#arrow-right')}}"></use>
                            </svg></span>
                        <span class="sii-m-badge-menu-item__link__title">Software development &amp; maintenance</span>
                    </a>
                </div>


            </div>
            <div class="sii-o-menu-rounded-large__list__item">
                <div class="sii-m-badge-menu-item ">
                    <a href="#" class="sii-m-badge-menu-item__link">
                        <span class="sii-a-badge-button -icon-default -invert sii-m-badge-menu-item__link__icon"><svg>
                                <use xlink:href="{{App\asset_path('svg/font-awesome-sprites/solid.svg#book')}}"></use>
                            </svg></span>
                        <span class="sii-a-badge-button sii-m-badge-menu-item__link__icon-hover"><svg>
                                <use xlink:href="{{App\asset_path('svg/font-awesome-sprites/solid.svg#arrow-right')}}"></use>
                            </svg></span>
                        <span class="sii-m-badge-menu-item__link__title">Cyber security</span>
                    </a>
                </div>


            </div>
            <div class="sii-o-menu-rounded-large__list__item">
                <div class="sii-m-badge-menu-item ">
                    <a href="#" class="sii-m-badge-menu-item__link">
                        <span class="sii-a-badge-button -icon-default -invert sii-m-badge-menu-item__link__icon"><svg>
                                <use xlink:href="{{App\asset_path('svg/font-awesome-sprites/solid.svg#book')}}"></use>
                            </svg></span>
                        <span class="sii-a-badge-button sii-m-badge-menu-item__link__icon-hover"><svg>
                                <use xlink:href="{{App\asset_path('svg/font-awesome-sprites/solid.svg#arrow-right')}}"></use>
                            </svg></span>
                        <span class="sii-m-badge-menu-item__link__title">Trainings</span>
                    </a>
                </div>


            </div>
            <div class="sii-o-menu-rounded-large__list__item">
                <div class="sii-m-badge-menu-item ">
                    <a href="#" class="sii-m-badge-menu-item__link">
                        <span class="sii-a-badge-button -icon-default -invert sii-m-badge-menu-item__link__icon"><svg>
                                <use xlink:href="{{App\asset_path('svg/font-awesome-sprites/solid.svg#book')}}"></use>
                            </svg></span>
                        <span class="sii-a-badge-button sii-m-badge-menu-item__link__icon-hover"><svg>
                                <use xlink:href="{{App\asset_path('svg/font-awesome-sprites/solid.svg#arrow-right')}}"></use>
                            </svg></span>
                        <span class="sii-m-badge-menu-item__link__title">Engineering</span>
                    </a>
                </div>


            </div>
            <div class="sii-o-menu-rounded-large__list__item">
                <div class="sii-m-badge-menu-item ">
                    <a href="#" class="sii-m-badge-menu-item__link">
                        <span class="sii-a-badge-button -icon-default -invert sii-m-badge-menu-item__link__icon"><svg>
                                <use xlink:href="{{App\asset_path('svg/font-awesome-sprites/solid.svg#bug')}}"></use>
                            </svg></span>
                        <span class="sii-a-badge-button sii-m-badge-menu-item__link__icon-hover"><svg>
                                <use xlink:href="{{App\asset_path('svg/font-awesome-sprites/solid.svg#arrow-right')}}"></use>
                            </svg></span>
                        <span class="sii-m-badge-menu-item__link__title">Testing services</span>
                    </a>
                </div>


            </div>
            <div class="sii-o-menu-rounded-large__list__item">
                <div class="sii-m-badge-menu-item ">
                    <a href="#" class="sii-m-badge-menu-item__link">
                        <span class="sii-a-badge-button -icon-default -invert sii-m-badge-menu-item__link__icon"><svg>
                                <use xlink:href="{{App\asset_path('svg/font-awesome-sprites/solid.svg#book')}}"></use>
                            </svg></span>
                        <span class="sii-a-badge-button sii-m-badge-menu-item__link__icon-hover"><svg>
                                <use xlink:href="{{App\asset_path('svg/font-awesome-sprites/solid.svg#arrow-right')}}"></use>
                            </svg></span>
                        <span class="sii-m-badge-menu-item__link__title">IT infrastructure</span>
                    </a>
                </div>


            </div>
            <div class="sii-o-menu-rounded-large__list__item">
                <div class="sii-m-badge-menu-item ">
                    <a href="#" class="sii-m-badge-menu-item__link">
                        <span class="sii-a-badge-button -icon-default -invert sii-m-badge-menu-item__link__icon"><svg>
                                <use xlink:href="{{App\asset_path('svg/font-awesome-sprites/solid.svg#book')}}"></use>
                            </svg></span>
                        <span class="sii-a-badge-button sii-m-badge-menu-item__link__icon-hover"><svg>
                                <use xlink:href="{{App\asset_path('svg/font-awesome-sprites/solid.svg#arrow-right')}}"></use>
                            </svg></span>
                        <span class="sii-m-badge-menu-item__link__title">Enterprise applications</span>
                    </a>
                </div>


            </div>
            <div class="sii-o-menu-rounded-large__list__item">
                <div class="sii-m-badge-menu-item ">
                    <a href="#" class="sii-m-badge-menu-item__link">
                        <span class="sii-a-badge-button -icon-default -invert sii-m-badge-menu-item__link__icon"><svg>
                                <use xlink:href="{{App\asset_path('svg/font-awesome-sprites/solid.svg#book')}}"></use>
                            </svg></span>
                        <span class="sii-a-badge-button sii-m-badge-menu-item__link__icon-hover"><svg>
                                <use xlink:href="{{App\asset_path('svg/font-awesome-sprites/solid.svg#arrow-right')}}"></use>
                            </svg></span>
                        <span class="sii-m-badge-menu-item__link__title">Business &amp; IT processes outsourcing</span>
                    </a>
                </div>


            </div>
            <div class="sii-o-menu-rounded-large__list__item">
                <div class="sii-m-badge-menu-item ">
                    <a href="#" class="sii-m-badge-menu-item__link">
                        <span class="sii-a-badge-button -icon-default -invert sii-m-badge-menu-item__link__icon"><svg>
                                <use xlink:href="{{App\asset_path('svg/font-awesome-sprites/solid.svg#book')}}"></use>
                            </svg></span>
                        <span class="sii-a-badge-button sii-m-badge-menu-item__link__icon-hover"><svg>
                                <use xlink:href="{{App\asset_path('svg/font-awesome-sprites/solid.svg#arrow-right')}}"></use>
                            </svg></span>
                        <span class="sii-m-badge-menu-item__link__title">Business intelligence</span>
                    </a>
                </div>


            </div>
            <div class="sii-o-menu-rounded-large__list__item">
                <div class="sii-m-badge-menu-item ">
                    <a href="#" class="sii-m-badge-menu-item__link">
                        <span class="sii-a-badge-button -icon-default -invert sii-m-badge-menu-item__link__icon"><svg>
                                <use xlink:href="{{App\asset_path('svg/font-awesome-sprites/solid.svg#book')}}"></use>
                            </svg></span>
                        <span class="sii-a-badge-button sii-m-badge-menu-item__link__icon-hover"><svg>
                                <use xlink:href="{{App\asset_path('svg/font-awesome-sprites/solid.svg#arrow-right')}}"></use>
                            </svg></span>
                        <span class="sii-m-badge-menu-item__link__title">Business consulting</span>
                    </a>
                </div>


            </div>
            <div class="sii-o-menu-rounded-large__list__item">
                <div class="sii-m-badge-menu-item ">
                    <a href="#" class="sii-m-badge-menu-item__link">
                        <span class="sii-a-badge-button -icon-default -invert sii-m-badge-menu-item__link__icon"><svg>
                                <use xlink:href="{{App\asset_path('svg/font-awesome-sprites/solid.svg#book')}}"></use>
                            </svg></span>
                        <span class="sii-a-badge-button sii-m-badge-menu-item__link__icon-hover"><svg>
                                <use xlink:href="{{App\asset_path('svg/font-awesome-sprites/solid.svg#arrow-right')}}"></use>
                            </svg></span>
                        <span class="sii-m-badge-menu-item__link__title">Remote work</span>
                    </a>
                </div>


            </div>
        </div>
    </div>
</div>
