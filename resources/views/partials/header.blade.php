<section class="sii-t-section u-move-over">
    <div class="container">
        <div class="sii-o-top-lead">

            <div class="sii-o-nav-bar">
                {{ sii_brand_logo() }}

                <div class="sii-o-nav-bar__item">



                    <div class="sii-o-nav-bar__item__nav-top">

                        <div class="sii-o-nav-bar__item__nav-top__language-select">{!!do_action('wpml_add_language_selector')!!}</div>

                        <a href="#" class="sii-a-button -small -secondary">
                            {{sii_text_settings( 'contact_button_', __('Contact Us', 'sii'))}}
                        </a>


                    </div>
                    <div class="sii-o-nav-bar__item__nav-main">
                        @if (has_nav_menu('main-menu'))
                            {!! sii_nav_menu(['theme_location' => 'main-menu', 'menu_class' => '-main-menu
                            -invert', 'depth' => 1])
                            !!}
                        @endif
                    </div>
                </div>
            </div>


            <div class="sii-o-top-lead__header">
                <div class="sii-o-top-lead__header__text u-move-over">
                    <h2 class="sii-o-top-lead__header__text__title sii-a-heading -h0">{{sii_text_settings( 'home_page_title' ) }}</h2>
                    <p class="sii-a-heading -t1 sii-o-top-lead__header__text__subtitle">{{sii_text_settings( 'home_page_describe' ) }}</p>
                    <div class="sii-m-btn-group ">
                        <li class="sii-m-btn-group__item">
                            <a href="#" class="sii-a-button -secondary">
                                {{sii_text_settings( 'home_page_btn_request', __('Send Your Request', 'sii') ) }}
                            </a>


                        </li>
                        <li class="sii-m-btn-group__item">
                            <a href="#" class="sii-a-button -primary">
                                {{sii_text_settings( 'home_page_btn_join', __('Join Sii', 'sii') ) }}
                            </a>


                        </li>
                    </div>
                    <a href="#" class="sii-o-top-lead__header__text__go-down sii-a-badge-button -only-icon -invert">
                        <svg class="icon"><use xlink:href="{{sii_icon_path('arrow-circle-o-down')}}"></use></svg>
                    </a>
                </div>
                <div class="sii-o-top-lead__header__utilities">

                    <span class="sii-o-top-lead__header__utilities__caption">
                        {{sii_text_settings( 'home_page_caption_text', __('Remek, Test & Analysis Engineer', 'sii') ) }}
                    </span>



                </div>
            </div>
        </div>

    </div>
</section>