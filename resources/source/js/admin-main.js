(function ($) {

// Use this variable to set up the common and page specific functions. If you
// rename this variable, you will also need to rename the namespace below.
    var bstarter = {
        // All pages
        common: {
            init: function () {

                this.expandNavbarOnClickSearch();
                this.animateSection($('.smartlib-animate-object'));
                this.counterUp($('.smartlib-counter'));
                this.chartAnimation($('.smartlib-chart-bar'));
                this.add_pretty_photo_gallery();//add pretty photo gallery
                this.addParalaxEffect();
                this.displayGooleMap(); //add google maps
                this.add_flexy_slider();
                this.scrollToElement();
                this.scrollToTop();
                this.tab_widget();
                this.threeLevelMenuBootstrap();
                /*responsive section on load document*/
                this.responsive_section();
                this.addDirectionalHover(); //direction-aware hover effect with css3 and jquery


            },


            animateSection: function (items, trigger) {


                items.each(function () {
                    var osElement = $(this),
                        osAnimationClass = osElement.attr('data-os-animation'),
                        osAnimationDelay = osElement.attr('data-os-animation-delay');

                    osElement.css({
                        '-webkit-animation-delay': osAnimationDelay,
                        '-moz-animation-delay': osAnimationDelay,
                        'animation-delay': osAnimationDelay
                    });


                    /*add opacity 0 to all animated sections - if css animations are supported*/
                    if (typeof osAnimationClass !== typeof undefined && osAnimationClass !== false && Modernizr.cssanimations !== false) {
                        osElement.css({
                            'opacity': 0
                        });
                    }

                    var osTrigger = ( trigger ) ? trigger : osElement;

                    osTrigger.waypoint(function () {
                        osElement.addClass('animated').addClass(osAnimationClass);
                    }, {
                        triggerOnce: true,
                        offset: '80%'
                    });
                });
            },
            counterUp: function (cunterObj) {

                if (cunterObj.length > 0) {

                    //reset box value
                    cunterObj.text(0);

                    cunterObj.waypoint(function () {

                        if (!cunterObj.hasClass('smartlib-counter-end')) {
                            cunterObj.countTo({speed: 2100});
                            cunterObj.addClass('smartlib-counter-end');
                        }
                    }, {
                        offset: '90%'
                    });

                }

            },
            chartAnimation:     function(chartBarObj) {

                if (chartBarObj.length > 0) {

                    chartBarObj.each(function () {

                        var $charBar = $(this);

                        $(this).waypoint(function () {

                            var $parentContainer = $charBar.parent('.smartlib-chart-box');

                            var dataBar = $charBar.attr('data-to');
                            var width = dataBar+'%';

                            $charBar.animate({
                                width: width
                            }, 1200, function () {

                            });
                            //$charBar.css('height', height);
                        }, {
                            offset: '90%'
                        });

                    });

                }
            },
            expandNavbarOnClickSearch: function () {
                $('.smartlib-navbar-search-form .smartlib-search-btn').on('click', function (e) {

                    var form_container = $('.smartlib-navbar-search-form');
                    if (!form_container.hasClass('smartlib-expanded-search-form')) {
                        form_container.addClass('smartlib-expanded-search-form animated fadeIn');

                        e.preventDefault();
                    }

                });
                $('.smartlib-navbar-search-form .smartlib-search-close-form').on('click', function (e) {

                    var form_container = $('.smartlib-navbar-search-form');
                    if (form_container.hasClass('smartlib-expanded-search-form')) {

                        form_container.removeClass('smartlib-expanded-search-form animated fadeIn');
                    }
                    e.preventDefault();
                });


            },
            responsive_section: function () {

                var window_width = $(window).width();


                $('.smartlib-responsive-section').each(function (index) {
                    var section_container = $(this);
                    var propotions = section_container.attr('data-proportions');

                    if (typeof propotions !== typeof undefined && propotions !== false) {
                        section_container.height(window_width * propotions);
                    }
                });

                // mobile padding

                $('.smartlib-blck-area').each(function (index) {

                        var section_container = $(this);
                        var attr = section_container.attr('data-mobile-padding');



                    if(window_width< 480){



                        var desktop_padding = section_container.css('padding');

                        if(typeof attr !== typeof undefined && attr !== false){

                            section_container.css('padding', attr);
                            section_container.attr('data-mobile-padding', desktop_padding);

                        }
                    }else{


                        var mobile_padding = section_container.css('padding');

                        if(typeof attr !== typeof undefined && attr !== false){

                            section_container.css('padding', attr);
                            section_container.attr('data-mobile-padding', mobile_padding);

                        }

                    }


                });


            },




            add_pretty_photo_gallery: function () {

                if ($("a[rel^='smartlib-resize-photo']").length > 0) {

                    $("a[rel^='smartlib-resize-photo']").prettyPhoto();
                }

            },
            add_flexy_slider: function () {

                var $slider = $('.u-flexslider');

                if ($slider.length > 0) {

                    $slider.each(function () {
                            $(this).flexslider();

                    });
                }

            },

            tab_widget: function(){

                if(jQuery('.smartlib-tab-content .smartlib-tab-single-content').length>0 ){

                    jQuery('.smartlib-tab-content .smartlib-tab-single-content:first-child').show();
                    jQuery('.smartlib-tab-menu li:first-child').addClass('smartlib-active-tab');

                    jQuery('.smartlib-tab-menu a').on('click', function(e)  {
                        var currentAttrValue = jQuery(this).attr('href');

                        // Show/Hide Tabs
                        jQuery('.smartlib-tab-content ' + currentAttrValue).show().siblings().hide();

                        // Change/remove current tab to active
                        jQuery(this).parent('li').addClass('smartlib-active-tab').siblings().removeClass('smartlib-active-tab');

                        e.preventDefault();
                    });

                }


            },

            addParalaxEffect: function () {

                // cache the window object
                $window = $(window);

                $('div[data-type="background"], section[data-type="background"]').each(function () {
                    // declare the variable to affect the defined data-type
                    var $scroll = $(this);
                    var bg_color = $scroll.attr('data-overlay-color');


                    //get rgb color

                    var patt = /^#([\da-fA-F]{2})([\da-fA-F]{2})([\da-fA-F]{2})$/;
                    var matches = patt.exec(bg_color);
                    if(matches){
                        var rgba = "rgba("+parseInt(matches[1], 16)+","+parseInt(matches[2], 16)+","+parseInt(matches[3], 16)+","+0.8+")";

                        //set rgb color
                        $scroll.css('background-color', rgba);
                    }


                });  // end section function

            },

            scrollToElement: function(){

                $('body').scrollspy({ target: '#smartlib-spy-scroll-nav', offset: 50 });

                $("#smartlib-one-page-menu a").on('click', function (){





                    var $container =  $(this);
                    var $parent_container = $container.parents('#smartlib-one-page-menu');
                    var containerTo = $container.attr('href');
                    var offset = 1* $parent_container.data('scroll-offset');




                    $('html, body').animate({
                        scrollTop: $(containerTo).offset().top - offset
                    }, 2000);

                });

            },

            scrollToTop: function(){

                $btnTop = $('#scroll-top-top');

                //Check to see if the window is top if not then display button
                $(window).scroll(function(){
                    if ($(this).scrollTop() > 100) {
                        $btnTop.addClass('slideInUp');
                    } else {
                        $btnTop.removeClass('slideInUp');
                    }
                });

                //Click event to scroll to top
                $btnTop.click(function(){
                    $('html, body').animate({scrollTop : 0},800);
                    return false;
                });

            },

            threeLevelMenuBootstrap: function(){

                $('ul.dropdown-menu [data-toggle=dropdown]').on('click', function(event) {
                    event.preventDefault();
                    event.stopPropagation();
                    $(this).parent().siblings().removeClass('open');
                    $(this).parent().toggleClass('open');
                });

            },

            displayGooleMap: function () {

                if ($('.smrtlib-google-maps').length > 0) {

                    $('.smrtlib-google-maps').each(function () {
                        var containerMap = $(this);
                        var markers = containerMap.find('.smartlib-map-marker');

                        var map = new GMaps({
                            scrollwheel: false,
                            div: '#' + containerMap.attr('id'),
                            zoom: containerMap.data('zoom'),
                            lat: containerMap.data('lat'),
                            lng: containerMap.data('long')
                        });


                        if (markers.length > 0) {

                            markers.each(function () {
                                var marker = $(this);

                                map.addMarker({
                                    lat: marker.data('lat'),
                                    lng: marker.data('long'),
                                    title: marker.data('text'),
                                    infoWindow: {
                                        content: '<p>' + marker.data('text') + '</p>'
                                    }

                                });
                            });
                        }

                    });
                }

            },

            addDirectionalHover: function(){

                $('.smrtlib-directional-hover-effect > li').directionalHover({

                    // CSS class for the overlay
                    overlay: "smartlib-caption-overlay",

                    // Linear or swing
                    easing: "swing",


                    // <a href="http://www.jqueryscript.net/animation/">Animation</a> speed in ms
                    speed: 400

                });

            },
            /**
             * Set contact map height based on form height
             */

            contactHomepageMapHeight: function () {

                var mapHeight = jQuery('.smartlib-homepage-form-column').height();

                jQuery('.smartlib-contact-iframe-map iframe').height(mapHeight);


            }




        },
        // Home
        home: {
            init: function () {

                bstarter.common.contactHomepageMapHeight();

            }
        },
        // About us page, note the change from about-us to about_us.
        about_us: {
            init: function () {
                // JavaScript to be fired on the about us page
            }
        },
        page_portfolio_isotope:{
            init: function () {
                this.portfolio_filter(); //add google maps

            },

            portfolio_filter: function () {

                var $portfolioContainer = $('.smartlib-layout-isotope-list');



                if($portfolioContainer.length>0){
                    $portfolioContainer.shuffle('shuffle');
                    $('.smartlib-sort-source li a').on('click', function (e) {
                        e.preventDefault();

                        $('.smartlib-sort-source li a').removeClass('smartlib-active-filter');

                        var isActive = $(this).hasClass( 'smartlib-active-filter' );
                        $(this).addClass('smartlib-active-filter');

                        var group = isActive ? 'all' :$(this).data('group');

                        $portfolioContainer.shuffle('shuffle', group );
                    });
                }
            }
        }
    };

// The routing fires all common scripts, followed by the page specific scripts.
// Add additional events for more control over timing e.g. a finalize event
    var UTIL = {
        fire: function (func, funcname, args) {
            var namespace = bstarter;
            funcname = (funcname === undefined) ? 'init' : funcname;
            if (func !== '' && namespace[func] && typeof namespace[func][funcname] === 'function') {
                namespace[func][funcname](args);
            }
        },
        loadEvents: function () {
            UTIL.fire('common');

            $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function (i, classnm) {

                UTIL.fire(classnm);
            });
        }
    };

    $(document).ready(UTIL.loadEvents);



    /*fire functions on window resize*/
    $(window).resize(function () {

        bstarter.common.responsive_section();

        bstarter.common.contactHomepageMapHeight();

    });

    $(window).load(function () {
        smartlib_preloader();

    });



    function smartlib_preloader() {

        imageSources = []
        $('img').each(function () {
            var sources = $(this).attr('src');
            imageSources.push(sources);
        });
        if ($(imageSources).load()) {
            $('.smartlib-pre-loader-area').fadeOut('slow');
        }
    }



    /*Fix double click Ipad*/

    $('body').on('touchstart','*',function(){   //listen to touch
        var jQueryElement=$(this);
        var element = jQueryElement.get(0); // find tapped HTML element
        if(!element.click){
            var eventObj = document.createEvent('MouseEvents');
            eventObj.initEvent('click',true,true);
            element.dispatchEvent(eventObj);
        }
    });
})(jQuery); // Fully reference jQuery after this point.
