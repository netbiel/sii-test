import $ from 'jquery';

class Person {
    constructor(fullName,favColor){
        this.name = fullName;
        this.favouriteColor = favColor;
        this.events();
    }
    events(){

    }
    greet() {
        console.log('Hello, my name is ' + this.name + ' and my favourite color is ' + this.favouriteColor + '.');
    }
}

export default Person;