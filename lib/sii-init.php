<?php

define('SII_THEME_BASE', dirname(dirname(__FILE__)));
define('SII_THEME_PATH', SII_THEME_BASE . '/resources/');
define('SII_VIEWS_PATH', SII_THEME_BASE . '/resources/views/');
define('SII_DIST_URI', get_template_directory_uri() . '/../assets/');




define('SII_THEME_SETTINGS', 'sii-sage/resourcess');
define('SII_THEME_LIB', SII_THEME_BASE . '/lib/');
define('SII_THEME_CONFIG', SII_THEME_BASE . '/config/');
define('SII_THEME_CLASSES', SII_THEME_LIB . '/classes/');
define('SII_THEME_VENDOR', SII_THEME_LIB . '/vendor/');
define('SII_THEME_RESOURCE_ASSETS',  get_template_directory_uri(). '/assets/');

/**
 * Load library files
 */

require_once SII_THEME_LIB . 'loader.php';

Sii::get_instance();

function sii_setup() {

    Sii::setup();

}

add_action('after_setup_theme', 'sii_setup');


/**
 * Parses dynamic blocks out of `post_content` and re-renders them.
 *
 * @since 5.0.0
 * @global WP_Post $post The post to edit.
 *
 * @param  string $content Post content.
 * @return string Updated post content.
 */


function sii_do_blocks($content)
{
    // If there are blocks in this content, we shouldn't run wpautop() on it later.
    $priority = has_filter('the_content', 'wpautop');
    if (false !== $priority && doing_filter('the_content') && has_blocks($content)) {
        remove_filter('the_content', 'wpautop', $priority);
        add_filter('the_content', '_restore_wpautop_hook', $priority + 1);
    }

    $blocks = parse_blocks($content);


    $output = '';

    foreach ($blocks as $block) {


        $output .= render_block($block);
    }

    return $output;
}


add_action('init', 'sii_do_blocks');

/*

function mytheme_block_templates($args, $post_type)
{

    global $post;
    var_dump($post->ID);
    var_dump(get_post_meta($post->ID, '_wp_page_template', true));

    //get_post_meta( $post->ID, '_wp_page_template', true );
    // Only add template to 'post' post type
    // Change for your post type: eg 'page', 'event', 'product'
    if ('page' == $post_type && is_page_template('views/template-custom.blade.php')) {
        // Optionally lock templates from further changes
        // Change to 'insert' to allow adding other blocks, but lock defined blocks
        $args['template_lock'] = 'all';
        // Set the template
        $args['template'] = [
            [
                // Example of including a core image block
                // Optional alignment setting
                'core/image', [
                'align' => 'left',
            ]
            ],
            [
                // Example of including a core paragraph block
                // Optional alignment placeholder setting
                'cgb/block-newsletter', [
                'placeholder' => 'The only thing you can add',
                'align' => 'right',
            ]
            ],
            [
                // Example of including a custom block
                // Optional placeholder setting
                'cgb/block-newsletter', [
                'placeholder' => 'Custom placeholder',
            ]
            ]
        ];
    }
    return $args;
}

// Hook function into post type arguments filter
//add_filter( 'register_post_type_args', 'mytheme_block_templates', 20, 2 );

//----------------------------

global $current_language;

$current_language = ICL_LANGUAGE_CODE;

function my_register_blogname_partials(WP_Customize_Manager $wp_customize)
{


    // Abort if selective refresh is not available.
    if (!isset($wp_customize->selective_refresh)) {
        return;
    }

    $wp_customize->selective_refresh->add_partial('home_page_title_' . ICL_LANGUAGE_CODE, [
        'selector' => '.sii-o-top-lead__header__text h2',
        'settings' => ['home_page_title_' . ICL_LANGUAGE_CODE],
        'render_callback' => function () {

            return get_theme_mod('home_page_title_' . ICL_LANGUAGE_CODE, 'display');
        },
    ]);

    $wp_customize->selective_refresh->add_partial('contact_button_' . ICL_LANGUAGE_CODE, [
        'selector' => '.sii-o-nav-bar__item__nav-top a',
        'settings' => ['contact_button_' . ICL_LANGUAGE_CODE],
        'render_callback' => function () {

            return get_theme_mod('contact_button_' . ICL_LANGUAGE_CODE);
        },
    ]);


}

add_action('customize_register', 'my_register_blogname_partials');


add_action('init', function () {


    Kirki::add_config('sii-sage/resources', array(
        'capability' => 'edit_theme_options',
        'option_type' => 'theme_mod',
    ));

    Kirki::add_panel('pages', array(
        'priority' => 10,
        'title' => esc_html__('Pages', 'kirki'),
        'description' => esc_html__('My panel description', 'kirki'),
    ));

    Kirki::add_section('home_page', array(
        'title' => esc_html__('Home Page', 'kirki'),
        'description' => esc_html__('My section description.', 'kirki'),
        'panel' => 'pages',
        'priority' => 160,
    ));

    Kirki::add_field('sii-sage/resources', [
        'type' => 'text',
        'settings' => 'contact_button_' . ICL_LANGUAGE_CODE,
        'label' => esc_html__('Text Control', 'my_textdomain'),
        'section' => 'home_page',
        'default' => esc_html__('Contact Us' . ICL_LANGUAGE_CODE, 'my_textdomain'),
        'priority' => 10,
        'partial_refresh' => [
            'contact_button_' . ICL_LANGUAGE_CODE => [
                'selector' => '.sii-o-nav-bar__item__nav-top a',
                'render_callback' => function () {

                    return get_theme_mod('contact_button_' . ICL_LANGUAGE_CODE, 'display');
                },
            ],

        ],

    ]);


    Kirki::add_field('sii-sage/resources', [
        'type' => 'text',
        'settings' => 'home_page_title_' . ICL_LANGUAGE_CODE,
        'label' => esc_html__('Text Control', 'my_textdomain'),
        'section' => 'home_page',
        'default' => esc_html__('Passion for technology', 'my_textdomain'),
        'priority' => 10,
        'partial_refresh' => [
            'home_page_title_' . ICL_LANGUAGE_CODE => [
                'selector' => '.sii-o-top-lead__header__text h2',
                'render_callback' => function () {

                    return get_theme_mod('home_page_title_' . ICL_LANGUAGE_CODE, 'display');
                },
            ],

        ],

    ]);

});

/**
 * Filtr do generowania partiali gutenberga z plików motywu
 * @param $partial
 * @param $block_name
 * @param $partial_params
 * @return string
 */

function sii_block_partial($partial, $block_name, $partial_params)
{

    if (file_exists(SII_VIEWS_PATH . '/partials/blocks/' . $block_name . '.blade.php')) {

        return '<div class="sii-editor-block">'. \App\template(SII_VIEWS_PATH . '/partials/blocks/'.$block_name.'.blade.php', ['block' => $partial_params,'partial' => $partial]).'</div>';

    }else {

        return $partial;

    }
}

add_filter('sii_block_partial', 'sii_block_partial', 2, 3);

/**
 * Get nav menu items by location
 *
 * @param $location The menu location id
 */
function get_nav_menu_items_by_location( $location, $args = [] ) {

    // Get all locations
    $locations = get_nav_menu_locations();







    // Get object id by location
    $object = wp_get_nav_menu_object( $locations[$location] );



    // Get menu items by menu name
    $menu_items = wp_get_nav_menu_items( $object->name, $args );

    // Return menu post objects
    return $menu_items;
}


$menu_sei = get_nav_menu_items_by_location('main-menu');

//var_dump($menu_sei);









