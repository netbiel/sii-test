<?php
use Carbon_Fields\Container;
use Carbon_Fields\Field;



if (!class_exists('SiiNavigation')) {

    /**
     * Root Class for initialize
     */
    class SiiNavigation extends SiiBase {

        public $post_types;
        public $taxonomies;
        public $actions;



        public function __construct($config) {

            parent::__construct($config);

            // Dodanie haków do kolekcji

            $this->hooks();

            // dodanie akcji z kolekcji do wordpressa

            $this->run_adds();



        }

        public function hooks() {

            $this->actions=[];

            // WordPress hook

            $this->add_action('wp_nav_menu_items',$this, 'action_search_link', 10, 2);


            // Theme Hooks

            //$this->add_action('sii_action_get_nav_menu_items_by_location',$this, 'action_get_nav_menu_items_by_location', 10, 1);



        }



        /**
         * Dodaje taxonomie do treści
         */

        public function action_search_link($items, $args) {



            if( $args->theme_location == 'main-menu' ){

                $items .= '<li class="sii-m-nav-menu__item"><a href="#" class="sii-m-nav-menu__link">'.SiiHelper::get_icon('search').'</a></li>';

            }

            return $items;

        }



    }

}