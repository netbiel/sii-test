<?php

if (!class_exists('SiiCustomizer')) {

    /**
     * Root Class for initialize
     */
    class SiiCustomizer extends SiiBase {



        public function __construct($config) {

            parent::__construct($config);




            if( class_exists('Kirki') ){

                Kirki::add_config( SII_THEME_SETTINGS, array(
                    'capability'    => 'edit_theme_options',
                    'option_type'   => 'theme_mod',
                ) );

                add_action('init', array( $this, 'add_panels'));
                add_action('init', array( $this, 'add_sections'));
                add_action('init', array( $this, 'add_fields' ));
                add_action('customize_register', array( $this, 'add_partial_refresh'));


            }

        }

        /**
         * Add Kirki Fields
         */

        public function add_panels() {

            $panels = $this->config->get_customizer_panels();

            foreach($panels as $key=>$panel) {

                Kirki::add_panel($key, $panel);

            }

        }

        /**
         * Add Kirki Sections
         */

        public function add_sections() {

            $sections = $this->config->get_customizer_sections();

            foreach($sections as $key=>$section) {

                Kirki::add_section($key, $section);

            }

        }

        /**
         * Add Kirki Fields
         */

        public function add_fields() {

            $fields = $this->config->get_customizer_fields();

            foreach($fields as $key=> $field) {
                Kirki::add_field(SII_THEME_SETTINGS, $field);


            }

        }

        public function add_partial_refresh($wp_customize) {

            // Abort if selective refresh is not available.
            if (!isset($wp_customize->selective_refresh)) {


                return;
            }




            $fields = $this->config->get_customizer_fields();

            foreach($fields as $field) {

                if(isset($field['partial_refresh'])) {


                    if(is_array($field['partial_refresh'])) {



                        foreach( $field['partial_refresh'] as $key => $partial) {


                            $wp_customize->selective_refresh->add_partial($key, [
                                'selector' => $partial['selector'],
                                'settings' => array($key),

                            ]);

                        }



                    }



                }


            }

        }




    }


}