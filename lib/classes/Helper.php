<?php


if (!class_exists('SiiHelper')) {

    /**
     * Class for getting config
     */
    class SiiHelper {

        private static $instance;



        /**
         * Get only one instance
         */
        public static function get_instance() {
            if (!isset( self::$instance )) {
                self::$instance = new self();
            }
            return self::$instance;
        }


        private function __construct() {

        }

        /**
         * Zwraca nazwe szablonu sekcji na bazie konfiguracji, w przypadku braku - nazwa domyślnej sekcji
         * @param array $config_array
         * @param $config_number
         * @return string
         */



        public static function section_template_path(array $config_array, $config_index ) {

            if( isset($config_array[$config_index]) ) {

                return SII_VIEWS_PATH . '/sections/'.$config_array[$config_index]['template-name'].'.blade.php';

            }else {

                return SII_VIEWS_PATH . '/sections/section-default.blade.php';

            }

        }


        public static function section_config_modyficators(array $config_array, $config_index ) {



            if( isset($config_array[$config_index]) ) {

                return $config_array[$config_index]['modyficators'] .' -template-'.$config_index;

            }else {

                return '-content-section -template-'.$config_index;

            }

        }

        /**
         * Zwraca sciezke do assetow w folderze resource/assets
         * @param $icon_name
         * @return string
         */

        public static function get_icon_path($icon_name) {

            return SII_THEME_RESOURCE_ASSETS. 'icons/svg/line-awesome/sprite.svg#'.$icon_name;

        }

        /**
         * Zwraca ścieżkę do obrazka na podstawie nazwy
         * @param $image_name
         * @return string
         */

        public static function get_image_path($image_name) {

            return SII_THEME_RESOURCE_ASSETS. 'images/'.$image_name;

        }


        /**
         * Zwraca komponent z ikonami
         * @param $icon_name
         * @return string
         */

        public static function get_icon($icon_name) {

            return '<svg><use xlink:href="'. self::get_icon_path($icon_name) .'"></use></svg>';

        }





    }
}