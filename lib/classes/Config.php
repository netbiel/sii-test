<?php


if (!class_exists('SiiConfig')) {

    /**
     * Class for getting config
     */
    class SiiConfig {

        private static $instance;
        private $default_settings;
        private $customizer_panels;
        private $customizer_sections;
        private $customizer_fields;
        private $theme_assets;
        public $theme_info;


        /**
         * Get only one instance
         */
        public static function get_instance() {
            if (!isset( self::$instance )) {
                self::$instance = new self();
            }
            return self::$instance;
        }


        private function __construct() {



            // default theme config

            //self::$default_settings =  include SII_THEME_CONFIG .'default-values.php';

            // customizer

            $this->customizer_panels = include SII_THEME_CONFIG .'customizer/panels.php';
            $this->customizer_sections = include SII_THEME_CONFIG .'customizer/sections.php';
            $this->customizer_partials = include SII_THEME_CONFIG .'customizer/partials.php';
            $this->customizer_fields = include SII_THEME_CONFIG .'customizer/fields.php';
            $this->post_types = include SII_THEME_CONFIG .'post-types.php';
            $this->custom_taxonomies = include SII_THEME_CONFIG .'custom-taxonomies.php';
            $this->theme_assets = include SII_THEME_CONFIG . 'sii/assets.php';
            $this->theme_fonts = include SII_THEME_CONFIG . 'sii/fonts.php';
            $this->theme_block_templates = include SII_THEME_CONFIG . 'block-templates.php';

            $this->theme_info = wp_get_theme();
            $this->default_values = include SII_THEME_CONFIG . 'default-values.php';

        }



        public function default_settings() {

            return $this->default_settings;

        }

        public function get_menus() {
            return $this->default_values['menus'];
        }



        public function get_customizer_panels() {

            return $this->customizer_panels;

        }

        public function get_customizer_sections() {

            return $this->customizer_sections;

        }

        public function get_customizer_fields() {


            return $this->add_fields_lang_sufix($this->customizer_fields);



        }

        public function get_customizer_partials() {

            return $this->customizer_partials;

        }

        // Zwiaca tablicę typów postów

        public function get_post_types() {

            return $this->post_types;

        }

        // Zwraca liste taxonomii

        public function get_taxonomies() {

            return $this->custom_taxonomies;

        }

        /**
         * Dodanie sufixu z językiem do pół
         */

        private function add_fields_lang_sufix ($fields_array) {

            $fields_modified = [];

            foreach ($fields_array as $base_key => $field) {

                $fields_modified[$base_key] = $field;

                switch ($base_key) {


                    case  $field['settings']:

                        $fields_modified[$base_key]['settings'] = $field['settings'] .'_'.SiiSettings::get_language();



                    case $field['partial_refresh']:

                        if(isset($field['partial_refresh']) && is_array($field['partial_refresh'])) {

                            foreach ($field['partial_refresh'] as $key => $partial) {

                                $fields_modified[$base_key]['partial_refresh'][$key  .'_'. SiiSettings::get_language()] = $partial;

                                unset($fields_modified[$base_key]['partial_refresh'][$key]);

                            }

                        }

                }


            }


            return $fields_modified;


        }

        /**
         * Return styles assets
         * @return bool
         */

        public function get_css_assets() {

            if (isset( $this->theme_assets[ 'styles' ] )) {
                return $this->theme_assets[ 'styles' ];
            } else {

                return false;

            }


        }

        /**
         * Return styles assets
         * @return bool
         */

        public function get_js_assets() {

            if (isset( $this->theme_assets[ 'scripts' ] )) {
                return $this->theme_assets[ 'scripts' ];
            } else {

                return false;

            }


        }

        /**
         * Return theme fonts from the config
         * @return mixed
         */

        public function get_fonts() {

            return $this->theme_fonts;

        }

        /**
         * Returns theme version
         * @return mixed
         */

        public function version() {

            return $this->theme_info->Version;

        }

        /**
         * Returns theme blocks templates
         * @return mixed
         */

        public function get_theme_blocks($page_template='') {

            if(strlen($page_template)>0){

                return isset($this->theme_block_templates[$page_template]) ? $this->theme_block_templates[$page_template]: array();

            } else {

                return $this->theme_block_templates;

            }


        }

    }
}