<?php
if (!class_exists('SiiBase')) {



    /**
     * Root Class for initialize
     */
    class SiiBase {


        public static $lang = 'pl';

        public $config;
        public $settings;
        protected static $instance;
        public $actions;
        public $filters;

        /**
         * Get only one instance
         */
        public static function get_instance($config) {

            if (! static::$instance instanceof static) {

                static::$instance = new static($config);

            }
            return static::$instance;
        }



        public function __construct($config) {

            $this->config = $config;

        }

        /**
         * A utility function that is used to register the actions and hooks into a single
         *
         * @param $hooks
         * @param $hook
         * @param $component
         * @param $callback
         * @param $priority
         * @param $accepted_args
         *
         * @return array
         */


        public function add( $hooks, $hook, $component, $callback, $priority, $accepted_args ) {

            $hooks[] = [ 'hook' => $hook, 'component' => $component, 'callback' => $callback, 'priority' => $priority, 'accepted_args' => $accepted_args ];

            return $hooks;

        }

        /**
         * Add action to global collection
         * @param $hook
         * @param $component
         * @param $callback
         * @param int $priority
         * @param int $accepted_args
         */

        protected function add_action( $hook, $component, $callback, $priority = 10, $accepted_args = 1 ) {
            $this->actions = $this->add($this->actions, $hook, $component, $callback, $priority, $accepted_args);
        }

        /**
         * Add filter to the global collection to be registered with WordPress
         * @param $hook
         * @param $component
         * @param $callback
         * @param int $priority
         * @param int $accepted_args
         */

        protected function add_filter( $hook, $component, $callback, $priority = 10, $accepted_args = 1 ) {

            $this->filters = $this->add($this->filters, $hook, $component, $callback, $priority, $accepted_args);

        }

        /**
         * Run the loader to execute all of the hooks with WordPress.
         *
         * @since    1.0.0
         */
        public function run() {
            $this->run_adds();
        }

        /**
         * Register the filters and actions with WordPress.
         *
         * @since    1.0.0
         */
        public function run_adds() {

            if(is_array($this->actions)) {

                foreach ($this->actions as $hook) {
                    add_action($hook['hook'], array($hook['component'], $hook['callback']), $hook['priority'], $hook['accepted_args']);
                }

            }

            if(is_array($this->filters)) {

                foreach ( $this->filters as $hook ) {

                    add_filter( $hook['hook'], array( $hook['component'], $hook['callback'] ), $hook['priority'], $hook['accepted_args'] );

                }

            }

        }





    }
}