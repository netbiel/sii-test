<?php

if (!class_exists('SiiContent')) {

    /**
     * Root Class for initialize
     */
    class SiiContent extends SiiBase {

        public $post_types;
        public $taxonomies;
        public $actions;



        public function __construct($config) {

            parent::__construct($config);

            // Dodanie haków do kolekcji

            $this->hooks();

            // dodanie akcji z kolekcji do wordpressa

            $this->run_adds();

        }

        public function hooks() {

            $this->actions=[];
            $this->add_action('init', $this, 'register_post_types');
            $this->add_action('init', $this, 'register_taxonomies');

        }

        /**
         * Dodaje typy postów
         */

        public function register_post_types() {

            $this->post_types = $this->config->get_post_types();

            foreach($this->post_types as $key => $post_type) {

                register_post_type($key, $post_type);

            }

        }

        /**
         * Dodaje taxonomie do treści
         */

        public function register_taxonomies() {

            $this->taxonomies = $this->config->get_taxonomies();

            foreach($this->taxonomies as $taxonomy => $taxonomy_config) {

                foreach($taxonomy_config as $post_type => $config ) {

                    register_taxonomy($taxonomy, $post_type, $config);

                }

            }

        }

    }

}