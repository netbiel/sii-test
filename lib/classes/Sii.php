<?php



if (!class_exists('Sii')) {

    /**
     * Root Class for initialize
     */
    class Sii {

        private static $instance;
        private static $theme_mods;
        public static $layout; // zmienna przechowująca obiekt layoutu
        public $config;
        public $settings;
        public $content;
        private $customizer;
        private static $setup;





        /**
         * Get only one instance
         */
        public static function get_instance() {

            if (!isset( self::$instance )) {

                self::$instance = new self();
            }

            return self::$instance;

        }


        private function __construct() {



            $this->config = SiiConfig::get_instance();
            $this->sttings = SiiSettings::get_instance($this->config);

            // Create theme classes and inject config & settings


            $this->customizer = SiiCustomizer::get_instance($this->config);

            $this->conent = SiiContent::get_instance($this->config);

            //main layout class
            self::$setup = SiiSetup::get_instance($this->config);
            self::$layout = SiiLayout::get_instance($this->config);

            SiiAssets::get_instance($this->config);
            SiiNavigation::get_instance($this->config);

        }



        public static function layout() {

            return self::$layout;

        }

        public static function setup() {

            self::$setup->theme_setup();

        }


    }
}