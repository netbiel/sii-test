<?php
if (!class_exists('SiiAssets')) {

    /**
     * Root Class for initialize
     */
    class SiiAssets extends SiiBase {

        public $actions;

        public $css_path;

        public $js_path;




        public function __construct( $config ) {

            parent::__construct($config);

            $this->css_path = SII_THEME_RESOURCE_ASSETS . 'css/';
            $this->js_path = SII_THEME_RESOURCE_ASSETS . 'js/';


            $this->hooks();

            $this->run_adds();

        }

        public function hooks() {

            $this->add_action('wp_enqueue_scripts', $this, 'enqueue_css_assets');
            $this->add_action('wp_enqueue_scripts', $this, 'enqueue_js_assets');
            //$this->add_action('admin_enqueue_scripts', $this, 'enqueue_css_admin');


            //$this->add_filter('body_class', $this, 'body_class_modyficator');


        }

        /**
         * Regster all scripts from the config file
         */

        private function register_scripts() {

            $scripts = $this->config->get_js_assets();

            if (is_array($scripts)) {

                foreach ($scripts as $key => $file) {

                    $handle = $key;
                    $src = $this->js_path . $file[ 'path' ];
                    $deps = isset( $file[ 'deps' ] ) ? $file[ 'deps' ] : array();
                    $in_footer = isset( $file[ 'in_footer' ] ) ? $file[ 'in_footer' ] : false;

                    wp_register_script($handle, $src, $deps, $this->config->version(), $in_footer);

                }

            }

        }

        /**
         * Rgester all styles from the config
         */

        private function register_styles() {

            //Regiset fonts firstly

            $this->register_fonts();

            $styles = $this->config->get_css_assets();

            if (is_array($styles)) {

                foreach ($styles as $key => $file) {

                    $handle = $key;
                    $src = $this->css_path . $file[ 'path' ];
                    $deps = isset( $file[ 'deps' ] ) ? $file[ 'deps' ] : array();
                    $media = isset( $file[ 'media' ] ) ? $file[ 'media' ] : false;

                    wp_register_style($handle, $src, $deps, $this->config->version(), $media);

                }

            }

        }

        public function enqueue_css_assets() {

            //1_ Register styles

            $this->register_styles();

            //2_ Enqueue styles
            wp_enqueue_style('main');


        }

        public function enqueue_js_assets() {

            $this->register_scripts();

            wp_enqueue_script('main');


        }

        public function enqueue_css_admin() {

            wp_register_style( 'custom_wp_admin_css', SII_DIST_URI . 'public.bundle.css', false, '1.0.0' );



            wp_enqueue_style('custom_wp_admin_css');
        }

        public function register_fonts() {

            $fonts = $this->config->get_fonts();



            if (is_array($fonts)) {

                foreach ($fonts as $handle => $font) {

                    if (!get_theme_mod($handle) || is_customize_preview()) {

                        //font handler same as option name

                        if(isset($font[1])) {

                            wp_register_style($handle, $font[1], array(), $this->config->version(), 'all');

                        }



                    }

                }

            }

        }




    }
}