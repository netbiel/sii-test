<?php

if (!class_exists('SiiSettings')) {

    /**
     * Class for getting config
     */
    class SiiSettings {

        private static $instance;

        private static $config;
        public static $lang_string = '';
        public static $lang = 'pl';

        /**
         * Get only one instance
         */
        public static function get_instance( $config ) {
            if (!isset( self::$instance )) {
                self::$instance = new self($config);
            }
            return self::$instance;
        }


        private function __construct( $config ) {

            self::$config = $config;


        }

        /**
         * Returns modified theme mod
         *
         * @param $key | meta or post meta key
         *
         * @return mixed
         */

        public static function get( $key, $default = false ) {

            global $post;

            $settings_local = self::_get_local_settings( $key, $default ); // local settings based on post meta

            $settings_global = get_theme_mod($key , $default);


            if (defined('ICL_LANGUAGE_CODE')) {

                self::$lang_string = '_' . ICL_LANGUAGE_CODE; // adds language sufix if language is defined

                $settings_global = get_theme_mod($key . self::$lang_string, $default);

            }

            return ( $settings_local !== false) ? $settings_local : $settings_global;

        }

        /**
         * Returns post meta fields
         * @param $key
         * @param bool $default
         *
         * @return bool|mixed
         */

        private static function _get_local_settings( $key, $default = false ) {

            global $post;


            if (function_exists('carbon_get_post_meta')) {

                $settings_local = carbon_get_post_meta($post->ID, $key);

            } else {

                $settings_local = get_post_meta($post->ID, $key, true);

            }


            if( isset($settings_local) && !empty($settings_local) ) {

                return $settings_local;

            } else {

                return $default;

            }

        }

        /**
         * Zwraca aktywny język w zależności od wtyczki
         * @return string
         */

        public static function get_language() {

            if (defined('ICL_LANGUAGE_CODE')) {

                self::$lang = ICL_LANGUAGE_CODE;

            }

            return self::$lang;

        }


    }
}

