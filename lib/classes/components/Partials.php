<?php



if (!class_exists('SiiPartials')) {

    /**
     * Klasa z kompoentami layoutu
     */
    class SiiPartials {


        private static $instance;



        /**
         * Get only one instance
         */
        public static function get_instance() {
            if (!isset( self::$instance )) {
                self::$instance = new self();
            }
            return self::$instance;
        }


        private function __construct() {



        }

        /**
         * Wyświetla pojedyńczy box z logo w stopce
         * @param $logo
         */

        public static function get_footer_logo_box($logo) {

            $src = wp_get_attachment_image_src($logo['logo_image'], 'full');

            if(strlen($src)>0){
                ?>

                <li class="sii-m-column-list__item"><img src="<?php echo $src[0] ?>" alt="logo" style="max-height: 50px" /></li>

            <?php

            }




        }

        /**
         * Komponent z Logo; TO DO: usprawnienia SEO, Nagłówek h4 w zalezności od podstrony
         * @param $class
         */

        public static function brand_logo($class) {
            ?>
            <h4 class="<?php echo $class ?>">
                <a class="<?php echo $class ?>-link"
                   href="<?php home_url('/') ?>">
                    <img src="<?php echo SiiHelper::get_image_path('logo-sii.png') ?>" width="122" height="79" alt="Sii Website" /></a>

            </h4>
            <?php
        }


        /**
         * Returns menu by location
         * @param $location
         * @param array $args
         * @return array|false
         */

        public static function action_get_nav_menu_items_by_location( $location, $args = [] ) {

            // Get all locations
            $locations = get_nav_menu_locations();

            // Get object id by location
            $object = wp_get_nav_menu_object( $locations[$location] );

            // Get menu items by menu name
            $menu_items = wp_get_nav_menu_items( $object->name, $args );

            // Return menu post objects
            return $menu_items;
        }


    }
}
