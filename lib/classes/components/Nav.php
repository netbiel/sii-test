<?php



if (!class_exists('SiiNav')) {

    /**
     * Root Class for initialize
     */
    class SiiNav {


        private static $instance;



        /**
         * Get only one instance
         */
        public static function get_instance() {
            if (!isset( self::$instance )) {
                self::$instance = new self();
            }
            return self::$instance;
        }


        private function __construct() {



        }

        public static function get_offer_menu($menu_object) {

            ?>

            <div class="sii-o-menu-rounded-large__list__item">
                <div class="sii-m-badge-menu-item ">
                    <a href="#" class="sii-m-badge-menu-item__link">
                        <span class="sii-a-badge-button -icon-default -invert sii-m-badge-menu-item__link__icon"><svg>
                                <use xlink:href="''"></use>
                            </svg></span>
                        <span class="sii-a-badge-button sii-m-badge-menu-item__link__icon-hover"><svg>
                                <use xlink:href="{{sii_icon_path('arrow-right')}}"></use>
                            </svg></span>
                        <span class="sii-m-badge-menu-item__link__title">Digital</span>
                    </a>
                </div>


            </div>

            <?php


        }


        /**
         * Returns menu by location
         * @param $location
         * @param array $args
         * @return array|false
         */

        public static function action_get_nav_menu_items_by_location( $location, $args = [] ) {

            // Get all locations
            $locations = get_nav_menu_locations();

            // Get object id by location
            $object = wp_get_nav_menu_object( $locations[$location] );

            // Get menu items by menu name
            $menu_items = wp_get_nav_menu_items( $object->name, $args );

            // Return menu post objects
            return $menu_items;
        }


    }
}
