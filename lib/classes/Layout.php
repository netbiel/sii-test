<?php

if (!class_exists('SiiLayout')) {

    /**
     * Root Class for initialize
     */
    class SiiLayout extends SiiBase
    {


        public function __construct($config)
        {

            parent::__construct($config);

            $this->hooks();

            $this->run_adds();


        }



        public function hooks()
        {

            // WordPress hooks


            // Theme hooks

            $this->add_filter('sii_body_style', $this, 'body_style');

            $this->add_filter('section_block_generator', $this, 'filter_section_block_generator', 1, 2);
            $this->add_filter('sii_filter_text_settings', $this, 'filter_text_settings', 1, 2);

            $this->add_action('sii_action_footer_logos', $this, 'action_footer_logos');
            $this->add_action('sii_brand_logo', $this, 'action_brand_logo');



        }

        /**
         * Modyfikuje style nagłówka : np. tlo
         * @return string
         */

        public function body_style($default)
        {

            global $post;

            $style = 'style="';




            if (!empty($default)) {

                $style .= $default . ';';

            }

            if (has_post_thumbnail()) {

                $photo = get_the_post_thumbnail_url($post->ID, 'full');

                $style .= 'background-image:  url(' . $photo . '),linear-gradient(180deg, rgba(238, 238, 238, 0) 0%, rgba(157, 157, 157, 0.34) 30.8%, #000000 100%);"';

            }

            return $style;
        }


        public function filter_section_block_generator($content, $page_template)
        {

            // If there are blocks in this content, we shouldn't run wpautop() on it later.
            $priority = has_filter('the_content', 'wpautop');

            $theme_blocks = $this->config->get_theme_blocks($page_template);

            if (false !== $priority && doing_filter('the_content') && has_blocks($content)) {
                remove_filter('the_content', 'wpautop', $priority);
                add_filter('the_content', '_restore_wpautop_hook', $priority + 1);
            }

            $blocks = parse_blocks($content);

            //var_dump($blocks);

            $output = '';

            foreach ($blocks as $key => $section) {

                $section_content = '';

                if (is_array($section['innerBlocks'])) {

                    foreach ($section['innerBlocks'] as $block) {

                        $section_content .= render_block($block);
                    }

                    $output .= \App\template(SiiHelper::section_template_path($theme_blocks, 'section_' . $key),
                        ['section_content' => $section_content,
                            'section_modyficators' => SiiHelper::section_config_modyficators($theme_blocks, 'section_' . $key)]);

                }


            }

            return $output;

        }

        /**
         * Zwraca ustawienia tekstu z customizera
         * @param $key
         * @param $default
         * @return mixed
         */

        public function filter_text_settings($key, $default)
        {

            return SiiSettings::get($key, $default);

        }

        /**
         * Wyświetla boxy logo w stopce na bazie ustawień customizera
         */

        public function action_footer_logos()
        {

            $logos = SiiSettings::get('footer_logos');

            if (is_array($logos)) {

                foreach ($logos as $logo) {

                    SiiPartials::get_footer_logo_box($logo);

                }

            }


        }

        public function action_brand_logo ($class) {

            SiiPartials::brand_logo($class);

        }


    }
}