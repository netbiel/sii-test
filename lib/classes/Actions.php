<?php
if (!class_exists('SiiSetup')) {

    /**
     * Root Class for initialize
     */
    class SiiActions extends SiiBase {








        public function theme_setup() {

            // Gutenberg wide images
            add_theme_support( 'align-wide' );

            // Add default posts and comments RSS
            add_theme_support( 'automatic-feed-links' );

            // Document title.
            add_theme_support( 'title-tag' );

            // Support for Post Thumbnails
            add_theme_support( 'post-thumbnails' );

            // This theme uses wp_nav_menu() in two locations.
            register_nav_menus(
                array(
                    'main-menu' => __( 'Main Menu', 'sii' ),
                    'footer' => __( 'Footer Menu', 'sii' ),
                    'social' => __( 'Social Links Menu', 'sii' ),
                )
            );

        }



    }
}