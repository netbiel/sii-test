<?php
use Carbon_Fields\Field;
use Carbon_Fields\Container;


if (!class_exists('SiiSetup')) {

    /**
     * Root Class for initialize
     */
    class SiiSetup extends SiiBase {

        public $menus;





        public function __construct($config) {

            parent::__construct($config);

            $this->menus = $this->config->get_menus();

            //Initialize Carbon fields
            \Carbon_Fields\Carbon_Fields::boot();

            Container::make('nav_menu_item', 'Menu Settings')
                ->add_fields(array(
                    Field::make('text', 'crb_color'),
                ));



        }


        public function theme_setup() {



            // Gutenberg wide images
            add_theme_support( 'align-wide' );

            // Add default posts and comments RSS
            add_theme_support( 'automatic-feed-links' );

            // Document title.
            add_theme_support( 'title-tag' );

            // Support for Post Thumbnails
            add_theme_support( 'post-thumbnails' );

            // This theme uses wp_nav_menu() in two locations.
            register_nav_menus(

                $this->menus

            );

        }



    }
}