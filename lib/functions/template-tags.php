<?php


/**
 * Funckcja wyświetlająca menu + dodanie walkera
 *
 * @param array $menu_args
 */

function sii_nav_menu(Array $menu_args)
{


    $preview_menu_args = $menu_args;

    if (!isset($menu_args['walker'])) {

        $menu_args['walker'] = new BEMWalkerNavMenu();

    }

        if(is_customize_preview()) {

            $preview_menu_args['container'] = 'div';

            $preview_menu_args['container_class'] = 'u-hide-customizer';

            wp_nav_menu($preview_menu_args);
        }

    wp_nav_menu($menu_args);

}

/**
 * Funkcja wyświetlająca Logo
 * @param string $class
 */

function sii_brand_logo($class = 'sii-o-nav-bar__brand-logo')
{
    do_action('sii_brand_logo', $class);
}

/**
 * Zwraca sciezke do ikon  w resource
 * @param $icon_name
 */

function sii_icon_path($icon_name) {

    echo SiiHelper::get_icon_path($icon_name);

}

