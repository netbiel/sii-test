<?php

/**
 * Funckcja obudowująca filtr do modyfikacji nagłówka stronie
 * @param string $default
 * @return mixed|void
 */

function sii_header_style($default='') {

    return apply_filters('sii_header_style', $default);

}

function sii_body_style($default='') {



    echo apply_filters('sii_body_style', $default);

}

function section_block_generator($content, $page_template) {

    echo apply_filters('section_block_generator', $content, $page_template);

}

function sii_action_get_nav_menu_items_by_location($menu_name) {

    do_action('sii_action_get_nav_menu_items_by_location', $menu_name);

}

/**
 * Wrapper do filtra z ustawieniami tekstu
 * @param $key
 * @param $default
 * @return mixed|void
 */

function sii_text_settings($key, $default=false) {

    return apply_filters('sii_filter_text_settings', $key, $default);

}

/**
 * Wyświetla box z logosami w stopce
 */

function sii_footer_logos() {

    do_action('sii_action_footer_logos');

}