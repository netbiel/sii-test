<?php

// vendors

require_once('vendor/kirki/kirki.php');
// Carbon Fields

require_once ('vendor/carbon-fields/vendor/autoload.php');
require_once('classes/BemMenuWalker.php');

// Sii Theme Classes

require_once('classes/Base.php');
require_once('classes/Config.php');
require_once('classes/Helper.php');

require_once('classes/components/Nav.php');
require_once('classes/components/Partials.php');

require_once('classes/Layout.php');
require_once('classes/Content.php');
require_once('classes/Navigation.php');
require_once('classes/Settings.php');
require_once('classes/Customizer.php');

require_once('classes/Assets.php');
require_once('classes/Setup.php');
require_once('classes/Sii.php');



// Sii Theme Functions
require_once('functions/template-hooks.php');
require_once('functions/template-tags.php');
require_once('functions/template-functions.php');

