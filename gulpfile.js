const gulp = require('gulp'),
	watch = require('gulp-watch'),
	svgSprite = require('gulp-svg-sprite'),
	del = require('del');

const config = {
	mode: {
		css: {
			sprite: 'sprite.svg',
			render: {
				scss: {
					template: './resources/source/sprite-generator/sprite-template.scss'
				}
			}
		}
	}
};

gulp.task('beginClean', function(){
	return del([
		'./assets/sprites',
		'resources/source/sprite-generator/css',
		'resources/source/scss/sprite/sprite.scss'
	]);
});

gulp.task('createSprite',['beginClean'],function(){
	return gulp.src('./resources/source/icons/**/*.svg')
		.pipe(svgSprite(config))
		.pipe(gulp.dest('resources/source/sprite-generator/'));
});

gulp.task('copySpriteStyles',['createSprite'],function(){
	return gulp.src('./resources/source/sprite-generator/css/*.scss')
		.pipe(gulp.dest('./resources/source/scss/sprite'));
});

gulp.task('copySpriteImage', ['createSprite'],function(){
	return gulp.src('resources/source/sprite-generator/css/**/*.svg')
		.pipe(gulp.dest('assets/sprites'));
});

gulp.task('sprite', ['beginClean','createSprite','copySpriteImage','copySpriteStyles']);

gulp.task('watch', function(){
	watch('resources/source/icons', function() {
		gulp.start('sprite');
	});
});